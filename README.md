# Secure Group File Sharing
This application consists of a file server (of which there may be multiple) and
a client (likewise) application implemented in Java. It supports group-based
permissions for file access and downloading. Users may create groups and add
other users to them to share files, which are uploaded to a server securely and
stored. Servers and users are authenticated by public key handshakes.

We use various secure protocols, some created by hand and others not (for
example, the Secure Remote Password protocol, which is used for authenticating
users by password), to protect against a variety of attacks. The [doc/](doc/)
directory contains complete information about each of these protocols and the
specific attacks we protect against.

The project's architecture consists of a user client, a file client and server,
and a group client and server. Their high-level relationship is shown in
[doc/DEV_NOTES.md](doc/DEV_NOTES.md).

Desired security properties were laid out at the beginning of the development
process in [the phase 1 write-up](doc/phase1-writeup.htm).

Phase 2 was development-intensive and did not have a corresponding write-up.
Its goal was essentially to achieve a working but wholly insecure file-sharing
system, with all security features to be implemented in later phases.

In phase 3, we implemented security features to protect against unauthorized
token issuance, token modification and forgery, unauthorized file servers,
and information leakage via passive monitoring (man-in-the-middle attacks).
Our approaches to each of these threats are documented in the [phase 3
write-up](doc/phase3-writeup.htm).

In phase 4, we implemented security features to protect against message
reordering, replaying, and modification attacks (active man-in-the-middle
attacks); data leaks at the file-server level (which effectively means ensuring
all files are encrypted prior to reaching a file server for storage); and token
theft. Our approaches to each of these threats are documented in the [phase 4
write-up](doc/phase4-writeup.htm).

In phase 5, we assessed a number of remaining threats which were not
addressed in previous phases, as well as the possible countermeasures,
some of which were implemented. These are documented in the [phase 5
write-up](doc/phase5-writeup.htm).

## Dependencies
This project was written for JDK 1.7 and likely will not compile in older
versions of Java. The [Bouncy Castle](https://bouncycastle.org/) library was
used as the provider for the Java cryptography API.
