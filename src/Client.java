import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public abstract class Client {

	/* protected keyword is like private but subclasses have access
	 * Socket and input/output streams
	 */
	protected Socket sock;
	protected ObjectOutputStream output;
	protected ObjectInputStream input;

	public Client() {
		Runtime.getRuntime().addShutdownHook(
			new Thread() {
				public void run() {
					try {
						if (output != null)
							output.close();
						if (input != null)
							input.close();
						if (sock != null)
							sock.close();
					}
					catch (IOException e) { }
				}
			}
		);
	}

	public boolean connect(final String server, final int port) {
		System.out.println("Attempting to connect...");
		if (server == null) {
			System.err.println("connect() : Server should not be null");
			return false;
		}

		try {
			sock = new Socket(server, port);
		}
		catch (IOException e) {
			// System.err.println(e.getMessage());
			return false;
		}

		System.out.printf("Connected to %s on port %d%n", server, port);

		try {
			output = new ObjectOutputStream(sock.getOutputStream());
			input  = new ObjectInputStream(sock.getInputStream());
		}
		catch (IOException e) {
			// System.err.println(e.getMessage());
			return false;
		}

		return true;
	}

	public boolean isConnected() {
		return sock != null && sock.isConnected();
	}

	public String getErrorMessage(Envelope e)
	{
		if (e.hasObjContents(1))
			return (String)e.getObjContents().get(0);
		switch (e.getMessage())
		{
			case "FAIL-BADTOKEN":
				return "Your token probably expired, or was otherwise corrupted.";
			case "FAIL-FILEEXISTS":
				return "File already exists.";
			case "FAIL-BADCONTENTS":
				return "There was a problem with the contents of the packet.";
			case "FAIL-BADGROUP":
				return "There was a problem with the group designator.";
			case "FAIL-BADREQ":
				return "There was a problem with the requester information.";
			case "FAIL-BADPATH":
				return "There was a problem with the path.";
			case "FAIL-UNAUTHORIZED":
				return "You are not authorized to do that.";
			case "FAIL-USEREXISTS":
				return "User already exists.";
			case "FAIL":
				return "Unknown error.";
			default:
				return "Bad response.";
		}
	}
}
// vim:noet:
