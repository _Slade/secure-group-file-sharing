public class ReadException extends Exception
{
    static final long serialVersionUID = -7182188678814389157L;

    public ReadException() { }

    public ReadException(String message)
    {
        super(message);
    }

    public ReadException(Throwable cause)
    {
        super(cause);
    }

    public ReadException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
