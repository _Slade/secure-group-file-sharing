import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class GroupApp implements Runnable {
    String server, username;
    int port;
    GroupClient gc;
    boolean keepRunning = true;
    Console input;
    int menuChoice;
    UserToken userToken;


    public GroupApp(String server, int port, UserToken userToken,
        GroupClient gc){
        this.server = server;
        this.port = port;
        this.userToken = userToken;
        this.gc = gc;
    }

    public void run(){
        input = System.console();
        if(input == null){
            System.err.println("Could not get system console");
            return;
        }
        ArrayList<String> usersGroups = new ArrayList<String>();
        if(gc.isConnected()){


            while(keepRunning) {
                System.out.println("Please select a number from the menu: " +
                        "\n\t1. Create a user" +
                        "\n\t2. Delete a user" +
                        "\n\t3. Create a group" +
                        "\n\t4. Delete a group" +
                        "\n\t5. List members of a group" +
                        "\n\t6. Add a user to a group" +
                        "\n\t7. Delete a user from a group" +
                        "\n\t8. Exit");

                try {
                    String in = input.readLine();
                    if (in == null)
                        keepRunning = false;
                    else
                        menuChoice = Integer.parseInt(in);
                }
                catch (NumberFormatException e) {
                    System.out.println("Could not understand input.");
                    continue;
                }

                if(menuChoice == 1){    //Create a user
                    System.out.print("Enter the username you'd like to create: ");
                    String newUsername = input.readLine();
                    if (newUsername == null){
                        continue;
                    }
                    char[] rawPwd = input.readPassword("Choose a password for this user: ");
                    if (rawPwd == null){
                        continue;
                    }
                    String pwd = new String(rawPwd);

                    if(gc.createUser(newUsername, pwd, userToken)){
                        System.out.println("User '" + newUsername + "' successfully created.");
                    }
                }

                else if(menuChoice == 2){   //Delete a user
                    System.out.print("Enter the username you'd like to delete: ");
                    String deleteUsername = input.readLine();
                    if (deleteUsername == null){
                        continue;
                    }

                    if(gc.deleteUser(deleteUsername, userToken)){
                        System.out.println("User '" + deleteUsername + "' successfully deleted.");
                    }
                }

                else if(menuChoice == 3){   //Create a group
                    System.out.print("Enter the group name to be created: ");
                    String createGroup = input.readLine();
                    if (createGroup == null){
                        continue;
                    }

                    if(gc.createGroup(createGroup, userToken)){
                        System.out.println("Group '" + createGroup + "' successfully created.");
                    }
                }

                else if(menuChoice == 4){   //Delete a group
                    System.out.print("Enter the name of the group you'd like to delete: ");
                    String deleteGroup = input.readLine();
                    if (deleteGroup == null){
                        continue;
                    }

                    if(gc.deleteGroup(deleteGroup, userToken)){
                        System.out.println("Successfully deleted group '" + deleteGroup + "'");
                    }
                }

                else if(menuChoice == 5){ //List members of a group
                    System.out.print("Enter a group name: ");
                    String group = input.readLine();
                    if (group == null){
                        continue;
                    }

                    List<String> members = gc.listMembers(group, userToken);
                    if (members != null){ //Group has to exist
                        if (members.size() == 0){
                            System.out.println("Group is empty.");
                        }

                        System.out.println("Members of '" + group + "':");

                        for(String name : members){
                            System.out.println("\t" + name);
                        }
                    }
                }

                else if(menuChoice == 6){   //Add a user to a group
                    System.out.print("Enter the username to add: ");
                    String username = input.readLine();
                    if (username == null){
                        continue;
                    }

                    System.out.print("\nEnter the group to add the user to: ");
                    String group = input.readLine();
                    if (group == null){
                        continue;
                    }

                    if(userToken.getGroups().contains(GroupServer.ADMIN)){
                        if(gc.addUserToGroup(username, group, userToken)){
                            System.out.println("'" + username + "' successfully added to '" + group + "'");
                        }
                    }
                }

                else if(menuChoice == 7){   //Delete user from group
                    System.out.print("Enter the group name: ");
                    String group = input.readLine();
                    if (group == null){
                        continue;
                    }

                    System.out.print("Enter the username: ");
                    String username = input.readLine();
                    if (username == null){
                        continue;
                    }

                    if(gc.deleteUserFromGroup(username, group, userToken)){
                        System.out.println("'" + username + "' successfully removed from group '" + group + "'");
                    }
                }

                else if(menuChoice == 8){ //Exit
                    keepRunning = false;
                }

                else {
                    System.out.println("Unknown Command.");
                }
            }
        }
    }
}
