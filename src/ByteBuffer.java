import java.util.*;
/**
 * A utility class for munging arrays of bytes. Similar to StringBuilder, but
 * for byte arrays rather than CharSequences.
 */
public class ByteBuffer
{
    Deque<byte[]> q;
    int totalSize = 0;

    public ByteBuffer()
    {
        q = new ArrayDeque<byte[]>();
    }

    public ByteBuffer(int initialCapacity)
    {
        q = new ArrayDeque<byte[]>(initialCapacity);
    }

    public ByteBuffer(Collection<byte[]> c)
    {
        q = new ArrayDeque<byte[]>(c);
    }

    public void append(byte[] buf)
    {
        if (buf == null)
            return;
        totalSize += buf.length;
        q.addLast(buf);
    }

    public void prepend(byte[] buf)
    {
        if (buf == null)
            return;
        totalSize += buf.length;
        q.addFirst(buf);
    }

    public void append(long l)
    {
        byte[] bytes = new byte[8];
        for (int i = 0; i < Long.SIZE / Byte.SIZE; ++i)
            bytes[i] = (byte)((l >> (i * Byte.SIZE)) & 0xFF);
        append(bytes);
    }

    public void clear()
    {
        q.clear();
    }

    /**
     * Flatten all byte arrays into a single, contiguous array.
     * @return The concatenation of all byte arrays added to this with append()
     * or prepend().
     */
    public byte[] flatten()
    {
        byte[] all = new byte[size()];
        int i = 0;
        for (byte[] buf : q)
        {
            System.arraycopy(buf, 0, all, i, buf.length);
            i += buf.length;
        }
        return all;
    }

    /**
     * Join a list of byte buffers together, separated by a delimiter.
     * @param delim The delimiter that goes between each joined element.
     * @param bufs The Iterable of CharSequences to be joined.
     */
    public void joinVar(byte[] delim, byte[]... bufs)
    {
        join(delim, bufs);
    }

    public void join(byte[] delim, byte[][] bufs)
    {
        byte[] d = new byte[] { };
        for (byte[] buf : bufs)
        {
            append(d);
            append(buf);
            d = delim;
        }
    }

    /** Get the total length of all byte arrays added to this buffer. */
    public int size()
    {
        return totalSize;
    }
}
