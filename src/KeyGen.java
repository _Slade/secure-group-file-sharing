import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Generates RSA key pairs and stores them on disk as pub.rsa and prv.rsa.
 * This class can be run with no arguments, in which case the keys are stored
 * in DEFAULT_PUB and DEFAULT_PRV. Alternatively, two arguments can be given to
 * specify the paths for the public and private keys, respectively.
 */
public class KeyGen
{
    private static final int     RSA_KEY_SIZE = 2048;
    private static final String  RSA          = "RSA",
                                 PROVIDER;
    public static final String DEFAULT_PUB = "pub.rsa";
    public static final String DEFAULT_PRV = "prv.rsa";
    public static final Console input         = System.console();
    // Set up security provider
    static
    {
        Provider p = new BouncyCastleProvider();
        Security.addProvider(p);
        PROVIDER = p.getName();
    }

    public static void main(String[] args) throws Exception
    {
        final File pubKeyFile, prvKeyFile;
        if (args.length == 0)
        {
            System.out.printf(
                "Using defaults: %s and %s%n",
                DEFAULT_PUB,
                DEFAULT_PRV
            );
            pubKeyFile = new File(DEFAULT_PUB);
            prvKeyFile = new File(DEFAULT_PRV);
        }
        else if (args.length == 2)
        {
            pubKeyFile = new File(args[0]);
            prvKeyFile = new File(args[1]);
        }
        else
        {
            System.out.println("Arguments not understood. Exiting.");
            return;
        }

        if (pubKeyFile.exists() || prvKeyFile.exists())
        {
            System.out.print("Key file(s) exist. Overwrite? [yn]: ");
            String rsp = input.readLine();
            if (rsp == null || !rsp.equals("y"))
                return;
        }
        initFile(pubKeyFile);
        initFile(prvKeyFile);
        // Public key file can be read by anyone, written to by owner (0644)
        pubKeyFile.setReadable(true, false); // chmod a+r
        pubKeyFile.setWritable(true, true);  // chmod u+w
        // Private key file can only be read/written by owner (0600)
        prvKeyFile.setReadable(true, true);  // chmod u+rw
        prvKeyFile.setWritable(true, true);
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(RSA, PROVIDER);
        keyGen.initialize(RSA_KEY_SIZE, new SecureRandom());
        KeyPair keys = keyGen.generateKeyPair();
        writeKey(keys.getPublic(), pubKeyFile);
        writeKey(keys.getPrivate(), prvKeyFile);
    }

    public static void initFile(File f) throws IOException
    {
        if (f.exists())
            f.delete();
        f.createNewFile();
        f.setExecutable(false, false); // chmod a-rwx
        f.setReadable(false, false);
        f.setWritable(false, false);
    }

    public static void writeKey(Key k, File f)
        throws IOException, FileNotFoundException
    {
        ObjectOutputStream oos = new ObjectOutputStream(
            new FileOutputStream(f)
        );
        oos.writeObject(k);
        oos.close();
    }

    public static KeyPair readKeys(File pubFile, File prvFile)
        throws IOException, FileNotFoundException, ClassNotFoundException
    {
        PublicKey pub;
        PrivateKey prv;
        ObjectInputStream ois;

        ois = new ObjectInputStream(new FileInputStream(pubFile));
        pub = (PublicKey)ois.readObject();
        ois.close();
        ois = new ObjectInputStream(new FileInputStream(prvFile));
        prv = (PrivateKey)ois.readObject();
        ois.close();
        return new KeyPair(pub, prv);
    }
}
