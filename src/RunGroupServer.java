/* Driver program for FileSharing Group Server */
import java.io.File;

public class RunGroupServer {
	/**
	 * Run a GroupServer. If no arguments are given, the server is run with
	 * default parameters. If exactly one argument is given, it is taken to be
	 * a port number. If exactly two arguments are given, they are taken to be
	 * the path of the public key and private key files, respectively. If
	 * exactly three arguments are given, they are taken to be the port number,
	 * public key path, and private key path, respectively.
	 */
	public static void main(String[] args) {
	    if (args.length == 0) {
			GroupServer server = new GroupServer();
			server.start();
	    }
		else if (args.length == 1) {
			try {
				GroupServer server = new GroupServer(Integer.parseInt(args[0]));
				server.start();
			}
			catch (NumberFormatException e) {
				System.out.printf(
                    "Enter a valid port number or pass no arguments to use the"
                    + " default port (%d)%n",
                    GroupServer.SERVER_PORT
				);
			}
		}
		else if (args.length == 2) {
            GroupServer server = new GroupServer(
                new File(args[0]),
                new File(args[1])
            );
            server.start();
		}
		else if (args.length == 3) {
		    try {
		        GroupServer server = new GroupServer(
		            Integer.parseInt(args[0]),
		            new File(args[1]),
		            new File(args[2])
		        );
		        server.start();
		    }
		    catch (NumberFormatException e) {
				System.out.printf(
                    "Enter a valid port number or pass no arguments to use the"
                    + " default port (%d)%n",
                    GroupServer.SERVER_PORT
				);
		    }
		}
		else {
		    System.out.printf(
                "Invalid number of arguments. Was expecting one of:%n"
                + "port%n"
                + "public_key private_key%n"
                + "port public_key private_key%n"
		    );
		    return;
		}
	}
}
