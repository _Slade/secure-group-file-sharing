public class SendException extends Exception
{
    static final long serialVersionUID = 440407671220963095L;

    public SendException() { }

    public SendException(String message)
    {
        super(message);
    }

    public SendException(Throwable cause)
    {
        super(cause);
    }

    public SendException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
