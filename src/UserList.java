import java.util.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * This class represents the list of users on the server.
 */
public class UserList implements java.io.Serializable
{

    private static final long serialVersionUID = 7600343803563417992L;

    private ConcurrentMap<String, Set<String>> userMemberships
        = new ConcurrentHashMap<String, Set<String>>();

    private ConcurrentMap<String, Set<String>> userOwnerships
        = new ConcurrentHashMap<String, Set<String>>();

    private ConcurrentMap<String, SRP> userSRPData
        = new ConcurrentHashMap<String, SRP>();

    private Set<String> groupNames
        = Collections.synchronizedSet(new HashSet<String>());

    private ConcurrentMap<String, ArrayList<byte[]>> groupKeys
        = new ConcurrentHashMap<String, ArrayList<byte[]>>();

    /**
     * Adds a user to this list of users.
     * @param user The user to add.
     * @param userSRP The user's stored SRP data (I, s, v).
     */
    public synchronized void addUser(String user, SRP userSRP)
    {
        assert user          != null : "Got null user";
        assert user.length() != 0    : "Got empty user";
        // Create a user who is a member of no groups and owns no groups
        userMemberships.put(
            user, Collections.synchronizedSet(new HashSet<String>())
        );
        userOwnerships.put(
            user, Collections.synchronizedSet(new HashSet<String>())
        );
        userSRPData.put(user, userSRP);
    }

    /**
     * Deletes a user from the userlist. This entails: (1) Deleting any
     * knowledge of the user's username; (2) deleting any knowledge of the
     * user's group memberships; (3) Removing the user from any groups he
     * is a member of; and (4) Deleting any groups he is an owner of.
     * @param user The name of the user to delete.
     */
    public synchronized void deleteUser(String user)
    {
        assert userExists(user) : "User \"" + user + "\" does not exist";
        // Remove user from all his groups
        userMemberships.remove(user);
        // Get all groups owned by this user
        Set<String> ownerships = userOwnerships.remove(user);
        for (String group : ownerships)
            deleteGroup(group);
    }

    /**
     * Get a list of groups that {@code user} is a member of. O(n) for a user
     * with n groups.
     */
    public synchronized List<String> getUserGroups(String user)
    {
        assert userExists(user) : "User \"" + user + "\" does not exist";
        List<String> ret = new ArrayList<String>();
        ret.addAll(userMemberships.get(user));
        return ret;
    }

    /**
     * Get a list of groups that {@code user} owns.
     */
    public synchronized List<String> getUserOwnership(String user)
    {
        assert userExists(user) : "User does not exist";
        List<String> ret = new ArrayList<String>();
        ret.addAll(userOwnerships.get(user));
        return ret;
    }

    /** Add {@code user} to the group {@code group}. */
    public synchronized void addGroup(String user, String group)
    {
        assert userExists(user)   : "User \""  + user  + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        assert !isMember(user, group) : "User is already in group";
        userMemberships.get(user).add(group);
    }

    /** Remove {@code user} from the group {@code group}. */
    public synchronized void removeGroup(String user, String group)
    {
        assert userExists(user)   : "User \""  + user  + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        assert isMember(user, group) : "User is not in group";
        // If the user owns the group, the whole group gets deleted.
        if (isOwner(user, group))
            deleteGroup(group);
        else
            userMemberships.get(user).remove(group);
    }

    /** Make {@code user} an owner of the group {@code group}. */
    public synchronized void addOwnership(String user, String group)
    {
        assert userExists(user)   : "User \""  + user  + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        assert !isOwner(user, group) : "User already owns group";
        userOwnerships.get(user).add(group);
    }

    /** Remove {@code user}'s ownership of the group {@code group}. */
    public synchronized void removeOwnership(String user, String group)
    {
        assert userExists(user)   : "User \""  + user  + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        assert isOwner(user, group) : "User does not own group";
        userOwnerships.get(user).remove(group);
    }

    /**
     * Checks if a group exists.
     * @param group The name of the group.
     * @return True iff the group exists.
     */
    public synchronized boolean groupExists(String group)
    {
        return groupNames.contains(group);
    }

    /**
     * Checks if a user exists.
     * @param user The user's username.
     * @return True iff the user exists.
     */
    public synchronized boolean userExists(String user)
    {
        assert userMemberships.containsKey(user)
            == userOwnerships.containsKey(user)
            : "userMemberships not consistent with userOwnerships";
        return userMemberships.containsKey(user);
    }

    /**
     * Creates a group.
     * @param group The name of the group.
     * @param user The name of the user to whom this group initially belongs.
     */
    public synchronized void createGroup(String group, String user)
    {
        // Don't want users to gain group ownership by re-creating existing groups
        assert !groupExists(group) : "Can't create group that already exists";
        assert userExists(user) : "User must exist to be made owner of group";

        groupNames.add(group);
        addOwnership(user, group);
        addGroup(user, group);

        byte[] groupKey = new CryptoManager().generateKey();
        ArrayList<byte[]> keys = new ArrayList<byte[]>();

        keys.add(groupKey);

        groupKeys.put(group, keys);
    }

    /**
     * Deletes a group and all user memberships in the group. It should be
     * verified that the user who requested this group to be deleted has the
     * permissions needed to do so before calling this method.
     * @param group The name of the group.
     */
    public synchronized void deleteGroup(String group)
    {
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        groupNames.remove(group);
        for (String user : userMemberships.keySet())
        {
            userMemberships.get(user).remove(group);
            userOwnerships.get(user).remove(group);
        }
        groupKeys.remove(group);
    }

    /**
     * Get a Set view of the groups in this UserList.
     */
    private synchronized Set<String> getGroups()
    {
        return groupNames;
    }

    /**
     * Checks if a user is the member of a group.
     * @param user The user to check.
     * @param group The group to check for membership in.
     * @return True iff the user is a member of the group.
     **/
    public synchronized boolean isMember(String user, String group)
    {
        assert userExists(user)   : "User \""  + user  + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        // Get the user from userNames, then get his group list and search it
        return userMemberships.get(user).contains(group);
    }

    /**
     * Checks if a user owns a group.
     * @param user The user.
     * @param group The group.
     * @return True iff the user owns the group.
     */
    public synchronized boolean isOwner(String user, String group)
    {
        assert userExists(user)   : "User \"" + user + "\" does not exist";
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        return userOwnerships.get(user).contains(group);
    }

    /**
     * Get the list of members in a group.
     * @param group The name of the group whose member list should be returned.
     * @return A reference to the list of members of the group.
     */
    public synchronized List<String> getGroupMembers(String group)
    {
        assert groupExists(group) : "Group \"" + group + "\" does not exist";
        List<String> members = new ArrayList<String>();
        for (String user : userMemberships.keySet())
            if (isMember(user, group))
                members.add(user);
        return members;
    }

    public synchronized SRP getSRP(String user)
    {
        assert userExists(user) : "User \"" + user + "\" does not exist.";
        return userSRPData.get(user);
    }

    /**
     * Get the number of users.
     */
    public synchronized int userCount()
    {
        return userMemberships.size();
    }

    /**
    *  get the key for the group
    */
    public synchronized byte[] getGroupKey(String group)
    {
        ArrayList<byte[]> keys = groupKeys.get(group);
        return keys.get(keys.size() - 1);
    }

    public synchronized byte[] getGroupKey(String group, int index)
    {
        return groupKeys.get(group).get(index);
    }

    public synchronized int getGroupKeyIndex(String group, byte[] key)
    {
        ArrayList<byte[]> keys = groupKeys.get(group);
        return keys.indexOf(key);
    }

    public void generateNewGroupKey(String group)
    {
        byte[] groupKey = new CryptoManager().generateKey();
        groupKeys.get(group).add(groupKey);
    }
} // End of UserList
