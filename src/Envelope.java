import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Envelope implements java.io.Serializable {

	private static final long serialVersionUID = -7726335089122193103L;
	private String msg;
	private List<Object> objContents = new ArrayList<Object>();
	private int sequenceNumber = -1;

	public Envelope(String text)
	{
		msg = text;
	}

	public String getMessage()
	{
		return msg;
	}

	public List<Object> getObjContents()
	{
		return objContents;
	}

	public Envelope addObject(Object object)
	{
		objContents.add(object);
		return this;
	}

	/* Ideally, this shouldn't be called except by the Shared classes. */
	public void setSequenceNumber(int i)
	{
		if (i < 0)
			throw new IllegalArgumentException(
				"Sequence number cannot be negative"
			);
		sequenceNumber = i;
	}

	public int getSequenceNumber()
	{
		if (sequenceNumber < 0)
			throw new RuntimeException("Sequence number not initialized");
		return sequenceNumber;
	}

    /**
     * Tests if this Envelope has {@code n} valid (existing and nonnull)
     * objects stored.
     * @param n The number of objects
     */
    public boolean hasObjContents(final int n)
    {
        if (objContents.size() < n)
            return false;
        for (int i = 0; i < n; ++i)
            if (objContents.get(i) == null)
                return false;
        return true;
    }

    public static byte[] toBytes(Envelope e) throws IOException
    {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out          = new ObjectOutputStream(bos))
        {
            out.writeObject(e);
            return bos.toByteArray();
        }
    }

    public static Envelope fromBytes(byte[] b)
        throws IOException, ClassNotFoundException
    {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(b);
             ObjectInput          in  = new ObjectInputStream(bis))
        {
            return (Envelope)in.readObject();
        }
    }

    public String toString()
    {
        return new StringBuilder()
            .append("Message: ")
            .append(getMessage())
            .append(System.lineSeparator())
            .append("Contents: ")
            .append(getObjContents().toString())
            .toString();
    }
}
