import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;

/**
 * FileServer loads files from FileList.bin, stores files in shared_files
 * directory.
 */
public class FileServer extends Server {

	public static final int SERVER_PORT = 4321;
	public FileList fileList;
	public PublicKey groupPubKey;
	public KeyPair keys;
	public byte[] fingerprint;
	public final File
		DEFAULT_PUB = new File("filePub.rsa"),
		DEFAULT_PRV = new File("filePrv.rsa");
	public static final boolean
		DEBUG = System.getProperty("DEBUG_FILE_CLIENT") != null
		     || System.getProperty("DEBUG_ALL")         != null;

	public FileServer() {
		super(SERVER_PORT, "FilePile");
	}

	public FileServer(int _port) {
		super(_port, "FilePile");
	}

	public void start() {
		String fileFile = "FileList.bin";
		ObjectInputStream fileStream;

		//This runs a thread that saves the lists on program exit
		Runtime runtime = Runtime.getRuntime();
		Thread catchExit = new Thread(new ShutDownListenerFS(this));
		runtime.addShutdownHook(catchExit);

		//Open user file to get user list
		try {
			FileInputStream fis = new FileInputStream(fileFile);
			fileStream = new ObjectInputStream(fis);
			fileList = (FileList)fileStream.readObject();
		}
		catch(FileNotFoundException e){
			System.out.println("FileList Does Not Exist. Creating FileList...");

			fileList = new FileList();
		}
		catch(IOException | ClassNotFoundException e){
			System.out.println(
				"Error reading from FileList file: " + e.getMessage()
			);
			System.exit(-1);
		}

		File file = new File("shared_files");
		if (file.mkdir()) {
			System.out.println("Created new shared_files directory");
		}
		else if (file.exists()){
			System.out.println("Found shared_files directory");
		}
		else {
			System.out.println("Error creating shared_files directory");
		}

		//Autosave Daemon. Saves lists every 5 minutes
		AutoSaveFS aSave = new AutoSaveFS(this);
		aSave.setDaemon(true);
		aSave.start();

		// Initiates authentication by first checking that the file server has a pair of RSA keys
		try {
			keys = KeyGen.readKeys(DEFAULT_PUB, DEFAULT_PRV);
		}
		catch (FileNotFoundException e) {
			System.out.println("Error when reading keys (maybe you need to run KeyGen): " + e.getMessage());
			System.exit(-1);
		}
		catch (Exception e) {
			System.out.println("Error when reading keys: " + e.getMessage());
			System.exit(-1);
		}

		// Load fingerprint for verifying tokens
		fingerprint = CryptoManager.getDigest().digest(
			keys.getPublic().getEncoded()
		);

		System.out.println("Loaded File Server's RSA keys");

		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("groupPub.rsa"));
			groupPubKey = (PublicKey)ois.readObject();
			ois.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Error when reading group server's public key (it may not have been manually added): " + e.getMessage());
			System.exit(-1);
		}
		catch (Exception e) {
			System.out.println("Error when reading keys: " + e.getMessage());
			System.exit(-1);
		}

		System.out.println("Loaded Group Server's public RSA key");

		boolean running = true;

		try {
			final ServerSocket serverSock = new ServerSocket(port);
			System.out.printf(
				"%s listening on port %d\n",
				this.getClass().getName(),
				port
			);

			Socket sock = null;
			Thread thread = null;

			while(running) {
				sock = serverSock.accept();
				thread = new FileThread(sock, this);
				thread.start();
			}

			System.out.printf("%s shut down\n", this.getClass().getName());
		}
		catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}

//This thread saves user and group lists
class ShutDownListenerFS implements Runnable
{
	private final FileServer fs;

	public ShutDownListenerFS(FileServer _fs)
	{
		fs = _fs;
	}

	public void run()
	{
		System.out.println("Shutting down server");
		ObjectOutputStream outStream;

		try
		{
			outStream = new ObjectOutputStream(new FileOutputStream("FileList.bin"));
			outStream.writeObject(fs.fileList);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}

class AutoSaveFS extends Thread
{
	private final FileServer fs;

	public AutoSaveFS(FileServer _fs)
	{
		fs = _fs;
	}

	public void run()
	{
		do
		{
			try
			{
				Thread.sleep(300000); //Save group and user lists every 5 minutes
				System.out.println("Autosave file list...");
				ObjectOutputStream outStream;
				try
				{
					outStream = new ObjectOutputStream(new FileOutputStream("FileList.bin"));
					outStream.writeObject(fs.fileList);
				}
				catch(Exception e)
				{
					System.err.println("Error: " + e.getMessage());
					e.printStackTrace(System.err);
				}

			}
			catch(Exception e)
			{
				System.out.println("Autosave Interrupted");
			}
		}while(true);
	}
}
