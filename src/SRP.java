import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;
import org.bouncycastle.crypto.agreement.srp.*;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.SRP6GroupParameters;
/**
 * User SRP parameters stored by the Group Server, plus a method for generating
 * per-user verifiers.
 * See doc/phase3-writeup.htm and SRP-6: Improvements and Refinements to
 * the Secure Remote Password Protocol, Thomas Wu, 2002 for notation and
 * description of the protocol.
 */
public class SRP implements Serializable
{
    static final long serialVersionUID = -5932031132658931899L;

    private String I;     // Username
    private byte[] s;     // Salt
    private BigInteger v; // Verifier
    public static final int SALT_SIZE = 16;

    /** The group parameters ({@code N}, {@code g}) used in this system. */
    public static final
        SRP6GroupParameters group = SRP6StandardGroups.rfc5054_1024;
    // Use the 1024 bit group values specified in RFC 5054.

    /**
     * Construct a new SRP object which stores the parameters that the Group
     * Server needs to know, namely {@code I}, {@code s}, and {@code v}.
     * This constructor generates {@code v} using the parameters passed into it.
     * @param username User's name.
     * @param password User's password.
     * @param rng The random number generator used to create the salt.
     * @param saltSize The size of the salt in bytes.
     * @throws NullPointerException if {@code I} or {@code s} are null.
     * @throws IllegalArgumentException If password or username are the empty
     * string, or if saltSize is not a positive value.
     */
    public SRP(String username, String password, SecureRandom rng, int saltSize)
    {
        Objects.requireNonNull(password, "password cannot be null");
        if (password.length() == 0)
            throw new IllegalArgumentException("password cannot be empty");
        Objects.requireNonNull(username, "username cannot be null");
        if (username.length() == 0)
            throw new IllegalArgumentException("username cannot be empty");
        Objects.requireNonNull(rng, "rng cannot be null");
        if (saltSize <= 0)
            throw new IllegalArgumentException("saltSize must be positive");

        // Store username
        I = username;
        // Store salt
        s = new byte[saltSize];
        rng.nextBytes(s); // Fill salt with random bytes

        // Get UTF-8 encoded byte arrays of the username and password
        byte[] rawPassword;
        byte[] rawUsername;
        rawPassword = stringToBytes(new String(password));
        rawUsername = stringToBytes(username);

        SRP6VerifierGenerator gen = new SRP6VerifierGenerator();
        gen.init(SRP.group, new SHA256Digest());
        // Store verifier
        v = gen.generateVerifier(s, rawUsername, rawPassword);
    }

    /** Get a copy of this user's salt. */
    public byte[] getSalt()
    {
        return Arrays.copyOf(s, s.length);
    }

    /** Get this user's verifier. */
    public BigInteger getVerifier()
    {
        return v;
    }

    public static byte[] stringToBytes(String s)
    {
        byte[] b;
        try { b = s.getBytes("UTF-8"); }
        catch (UnsupportedEncodingException e)
        {
            System.err.println(e.getMessage());
            System.exit(-1);
            return null;
        }
        return b;
    }
}
