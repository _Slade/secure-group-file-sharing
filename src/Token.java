import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.*;

public class Token implements UserToken, Serializable {
    static final long serialVersionUID = -3878841812937833326L;
    private String issuer, subject;
    private List<String> groups;
    private byte[] signature;

    // Time in milliseconds for a token to expire
    public static final long EXPIRATION_TIME = 60 * 60 * 1000;

    public Token(String issuer, String subject, List<String> groups){
        this.issuer = issuer;
        this.subject = subject;
        this.groups = groups;
    }

    public String getIssuer(){
        return issuer;
    }

    public String getSubject(){
        return subject;
    }

    public List<String> getGroups(){
        return groups;
    }

    public String toString(){
        return ("Issuer: " + issuer + ". Subject: " + subject + ". Groups: " + groups);
    }

    /**
     * Computes the canonical hash of this token. May be slow if the token has
     * many groups.
     * @param fingerprint The SHA-256 hash of the public key belonging to the
     * server that the subject wishes to communicate with.
     */
    private byte[] getCanonical(byte[] fingerprint) {
        final String enc = "UTF-8";
        final byte[] delim = { (byte)0xF4, (byte)0xF4, (byte)0xF4, (byte)0xF4 };
        ByteBuffer bytes = new ByteBuffer();
        // Get our encoded strings
        try {
            bytes.append(getIssuer().getBytes(enc));
            bytes.append(getSubject().getBytes(enc));
            bytes.append(fingerprint);
            bytes.append(System.currentTimeMillis() / EXPIRATION_TIME);
            for (String group : new TreeSet<String>(getGroups())) {
                bytes.append(group.getBytes(enc));
            }
        } catch (UnsupportedEncodingException e) {
            System.err.println(
                "Could not encode groups to UTF-8. This is a fatal problem"
                + " with the installation."
            );
            System.exit(-1);
            return null;
        }

        return bytes.flatten();
    }

    public boolean sign(PrivateKey key, byte[] fingerprint) {
        try {
            Signature baseSignature = Signature.getInstance("SHA256withRSA", "BC");
            baseSignature.initSign(key);
            baseSignature.update(getCanonical(fingerprint));
            byte[] signedSignature = baseSignature.sign();
            signature = signedSignature;
        } catch (java.security.GeneralSecurityException e) {
            signature = null;
            return false;
        }
        return true;
    }

    public boolean verifySignature(PublicKey key, byte[] fingerprint) {
        boolean valid = false;

        try {
            if(signature != null){
                Signature baseSignature = Signature.getInstance("SHA256withRSA", "BC");
                baseSignature.initVerify(key);
                baseSignature.update(getCanonical(fingerprint));

                if(baseSignature.verify(signature)) {
                    valid = true;
                }
            }
        } catch (java.security.GeneralSecurityException e) {
            valid = false;
        }

        return valid;
    }
}
