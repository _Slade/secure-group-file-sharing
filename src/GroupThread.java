import java.io.*;
import java.lang.Thread;
import java.math.BigInteger;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.*;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.agreement.srp.*;
import org.bouncycastle.crypto.digests.SHA256Digest;

/**
 * This thread does all the work. It communicates with the client through
 * Envelopes.
 */
public class GroupThread extends Thread
{
    private final Socket socket;
    private final GroupServer my_gs;
    private final UserList ul;
    // Contains stuff common to client and server
    private final CryptoManager cm;

    private static final String FAIL_MSG = "FAIL";
    private static final Envelope PARAM_ERROR = new Envelope(FAIL_MSG)
        .addObject("Error: Insufficient parameters");

    public GroupThread(Socket _socket, GroupServer _gs) throws IOException
    {
        socket   = _socket;
        my_gs    = _gs;
        ul       = my_gs.userList;
        cm       = new CryptoManager(
            new ObjectInputStream(socket.getInputStream()),
            new ObjectOutputStream(socket.getOutputStream())
        );
    }

    /**
     * Tries to read messages over and over, reporting runtime exceptions when
     * they occur.
     */
    public void run()
    {
        // Announces connection and opens object streams
        System.out.println(
            "*** New connection from " + socket.getInetAddress()
            + ":" + socket.getPort() + "***"
        );

        try { authenticate(); }
        catch (BadHandshakeException e)
        {
            if (GroupServer.DEBUG)
            {
                System.err.println(e.getMessage());
                e.printStackTrace(System.err);
            }
            disconnect();
            return;
        }
        catch (SendException | ReadException e)
        {
            System.err.println(
                "Something went really wrong: " + e.getMessage()
            );
            e.printStackTrace(System.err);
            disconnect();
            return;
        }
        catch (EOFException e)
        {
            System.err.println("Input closed.");
            disconnect();
            return;
        }

        int exceptionsInARow = 0;
        RUNLOOP:
        while (true)
        {
            try
            {
                if (socket.isClosed())
                    break RUNLOOP;
                if (exceptionsInARow > 5)
                {
                    System.err.println(
                        "Too many exceptions in a row in thread "
                        + Thread.currentThread().getId()
                        + ", shutting it down."
                    );
                    break RUNLOOP;
                }
                Envelope message = cm.readMessage();
                System.out.println("Request received: " + message.getMessage());
                Envelope response = processMessage(message);
                if (response == null) // Disconnected
                    break RUNLOOP;
                // Log failed actions in debug mode
                if (GroupServer.DEBUG && response.getMessage().equals(FAIL_MSG))
                    System.err.println(response.getObjContents().get(0));
                cm.sendMessage(response);
                exceptionsInARow = 0;
            }
            catch (EOFException e)
            {
                disconnect();
                break RUNLOOP;
            }
            catch (ReadException e)
            {
                System.err.println("Error reading message: " + e.getMessage());
                e.printStackTrace(System.err);
                ++exceptionsInARow;
            }
            catch (SendException e)
            {
                System.out.println("Error sending message: " + e.getMessage());
                e.printStackTrace(System.err);
                ++exceptionsInARow;
            }
        }
    }

    /**
     * Authenticate with the client and establish a secure connection.
     * @throws BadHandshakeException If the client's credentials or messages
     * were bad.
     */
    private void authenticate()
        throws BadHandshakeException, ReadException, SendException,
        EOFException
    {
        try
        {
            final BigInteger A;      // Client's half of the shared secret.
            final BigInteger B;      // Server's half of the shared secret.
            final BigInteger K;      // Final key derived from shared secret.
            final SRP userSRP;       // The user's (username, salt, verifier).
            final String username;   // Needed to get the userSRP.
            final SRP6Server server; /* Class that handles server-side SRP
                                        calculations */
            final BigInteger M1,       // Client proof
                             M2;       // Server proof
            final Envelope m1, m3, m5; /* Messages from client. Even-numbered
                                          messages are sent by us. */

            // MESSAGE 1: Receive username
            if (GroupServer.DEBUG)
                System.err.println("Waiting for message 1: username");
            m1 = cm.readMessagePlaintext();
            if (!m1.hasObjContents(1))
                throw new BadHandshakeException(
                    "Bad message 1: " + m1.toString()
                );

            username = (String)m1.getObjContents().get(0);

            // Check if client tried to log in as someone who doesn't exist
            if (!ul.userExists(username))
                throw new BadHandshakeException(
                    "Got unrecognized username: " + username
                );

            userSRP = ul.getSRP(username);
            // MESSAGE 2: Send salt
            cm.sendMessagePlaintext(
                new Envelope(null).addObject(userSRP.getSalt())
            );

            // MESSAGE 3: Receive A
            m3 = cm.readMessagePlaintext();
            if (!m3.hasObjContents(1))
                throw new BadHandshakeException("Bad message 3");

            A = (BigInteger)m3.getObjContents().get(0);

            // Safeguard: A must not equal 0 (mod N)
            if (A.mod(SRP.group.getN()) == BigInteger.ZERO)
                throw new BadHandshakeException(
                    "Error: Value of A violates safeguard"
                );

            // Generate our half of the shared key B, use it to derive the
            // shared secret K, then B along with a challenge M1 to the client.
            server = new SRP6Server();
            server.init(
                SRP.group,
                userSRP.getVerifier(),
                new SHA256Digest(),
                cm.getRandom()
            );

            // This is our half of the shared key
            B = server.generateServerCredentials();

            // Calculate our shared secret from A and B. If the client's half
            // was bad, this blows up with a CryptoException.
            server.calculateSecret(A);

            // MESSAGE 4: Send B and challenge M1
            cm.sendMessagePlaintext(new Envelope(null).addObject(B));

            // Verify the client's response to the challenge, and ONLY if it's
            // valid, send our own response to its challenge back.

            // MESSAGE 5: Receive message 5 containing client's solution
            m5 = cm.readMessagePlaintext();
            if (!m5.hasObjContents(1))
                throw new BadHandshakeException("Bad message 5");
            M1 = (BigInteger)m5.getObjContents().get(0);
            // Throws CryptoException if response was bad
            server.verifyClientEvidenceMessage(M1);
            // MESSAGE 6: Send M2, which the client uses to verify us, as well
            // as the IV used for the cipher.
            M2 = server.calculateServerEvidenceMessage();

            // Generate and set our IV
            byte[] iv = cm.generateIV();
            cm.setIV(iv);

            // Calculate session key using shared secret
            cm.initKey(server.calculateSessionKey());

            // Generate and encrypt integrity key
            byte[] integrityKey = cm.generateIntegrityKey();
            cm.setIntegrityKey(integrityKey);
            byte[] encryptedKey = cm.encrypt(integrityKey);
            assert encryptedKey != null
                && !Arrays.equals(integrityKey, encryptedKey)
                : "Problem encrypting integrity key";

            cm.sendMessagePlaintext(
                new Envelope(null)
                .addObject(M2)
                .addObject(iv)
                .addObject(encryptedKey)
            );
        }
        catch (ReadException | SendException e)
        {
            System.err.println(e.getMessage());
            disconnect();
            return;
        }
        catch (CryptoException e)
        {
            System.out.println("User had bad credentials.");
            disconnect();
            return;
        }
    }

    private void disconnect()
    {
        try { socket.close(); }
        catch (IOException e)
        {
            System.err.println("Could not disconnect:");
            e.printStackTrace(System.err);
        }
        System.out.println("Connection closed.");
    }

    /**
     * Processes a message and returns a response appropriate for sending to the
     * client.
     * @param message A message received from the client.
     */
    private Envelope processMessage(Envelope message)
    {
        switch (message.getMessage())
        {
            case "GET": // Client wants a token
            {
                if (!message.hasObjContents(1) && !message.hasObjContents(2))
                {
                    if (GroupServer.DEBUG)
                        System.err.println(
                            "The offending envelope is: " + message.toString()
                        );
                    return PARAM_ERROR;
                }
                String username = (String)message.getObjContents().get(0);
                byte[] fingerprint = message.hasObjContents(2)
                    ? (byte[])message.getObjContents().get(1)
                    : null;
                return getToken(username, fingerprint);
            }
            case "CUSER": // Client wants to create a user
            {
                if (!message.hasObjContents(3))
                    return PARAM_ERROR;

                String    username  = (String)message.getObjContents().get(0);
                String    password  = (String)message.getObjContents().get(1);
                UserToken yourToken = (UserToken)message.getObjContents().get(2);

                return createUser(username, password, yourToken);
            }
            case "DUSER": // Client wants to delete a user
            {
                if (!message.hasObjContents(2))
                    return PARAM_ERROR;

                String    username  = (String)message.getObjContents().get(0);
                UserToken yourToken = (UserToken)message.getObjContents().get(1);

                return deleteUser(username, yourToken);
            }
            case "CGROUP": // Client wants to create a group
            {
                if (!message.hasObjContents(2))
                    return PARAM_ERROR;

                String    groupname = (String)message.getObjContents().get(0);
                UserToken yourToken = (UserToken)message.getObjContents().get(1);

                return createGroup(groupname, yourToken);
            }
            case "DGROUP": // Client wants to delete a group
            {
                if (!message.hasObjContents(2))
                    return PARAM_ERROR;

                String    groupname = (String)message.getObjContents().get(0);
                UserToken yourToken = (UserToken)message.getObjContents().get(1);

                return deleteGroup(groupname, yourToken);
            }
            case "LMEMBERS": // Client wants a list of members in a group
            {
                if (!message.hasObjContents(2))
                    return PARAM_ERROR;

                String    groupname = (String)message.getObjContents().get(0);
                UserToken yourToken = (UserToken)message.getObjContents().get(1);

                return listMembers(groupname, yourToken);
            }
            case "AUSERTOGROUP": // Client wants to add user to a group
            {
                if (!message.hasObjContents(3))
                    return PARAM_ERROR;

                String    username  = (String)message.getObjContents().get(0);
                String    groupname = (String)message.getObjContents().get(1);
                UserToken yourToken = (UserToken)message.getObjContents().get(2);

                return addUserToGroup(username, groupname, yourToken);
            }
            case "RUSERFROMGROUP": // Client wants to remove user from a group
            {
                if (!message.hasObjContents(3))
                    return PARAM_ERROR;

                String    username  = (String)message.getObjContents().get(0);
                String    groupname = (String)message.getObjContents().get(1);
                UserToken yourToken = (UserToken)message.getObjContents().get(2);

                return removeUserFromGroup(username, groupname, yourToken);
            }
            case "GGROUPKEY": // Gets the AES key coresponding to the group
            {
                String    groupname = (String)message.getObjContents().get(0);
                UserToken yourToken = (UserToken)message.getObjContents().get(1);

                return getGroupKey(groupname, yourToken);
            }
            case "GGROUPKEYI":
            {
                String    groupname = (String)message.getObjContents().get(0);
                int       keyIndex  = (int)message.getObjContents().get(1);
                UserToken yourToken = (UserToken)message.getObjContents().get(2);

                return getGroupKey(groupname, keyIndex, yourToken);
            }
            default:
            {
                return new Envelope("FAIL").addObject(
                    "Server does not understand request"
                );
            }
        }
    } // End of processMessage

    /* METHODS FOR PERFORMING ACTIONS AND CONSTRUCTING APPROPRIATE RESPONSES */
    /* ===================================================================== */

    /**
     * Create a new user and return a response. All verification of created
     * usernames should be performed here.
     * @param username The username of the user to be created.
     * @param password This user's password.
     * @param yourToken The token belonging to a member of the ADMIN group.
     */
    private Envelope createUser(String username, String password, UserToken yourToken)
    {
        final String requester = yourToken.getSubject();

        if (GroupServer.DEBUG)
            System.err.println("Attempting to create user \"" + username + "\"");

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(requester))
            return new Envelope("FAIL-BADREQ").addObject(
                String.format(
                    "Cannot create new user \"%s\": requester \"%s\" does not exist.",
                    username,
                    requester
                )
            );

        if (!ul.isMember(requester, GroupServer.ADMIN))
            return new Envelope("FAIL-UNAUTHORIZED").addObject(
                String.format(
                    "Cannot create new user \"%s\": requester \"%s\" is not an administrator.",
                    username,
                    requester
                )
            );

        if (ul.userExists(username))
            return new Envelope("FAIL").addObject(
                "Cannot create new user \"" + username + "\": user already exists."
            );

        if (username.equals(""))
            return new Envelope("FAIL").addObject(
                "Refusing to create user with empty name."
            );

        ul.addUser(
            username,
            new SRP(username, password, cm.getRandom(), SRP.SALT_SIZE)
        );
        return new Envelope("OK");
    }

    /**
     * Deletes a user from this server.
     * @param username The username of the user to delete.
     * @param yourToken The token of an administrator.
     * @return True iff the token is that of an admin, the user exists, and was
     * successfully deleted.
     */
    private Envelope deleteUser(String username, UserToken yourToken)
    {
        String requester = yourToken.getSubject();
        Envelope response;

        if (GroupServer.DEBUG)
            System.err.println("Attempting to delete user \"" + username + "\"");

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(requester)) // Token must belong to a real user
            return new Envelope("FAIL").addObject(
                "User \"" + requester + "\" does not exist."
            );

        if (!ul.userExists(username)) // User must exist to be deleted
            return new Envelope("FAIL").addObject(
                "User \"" + username + "\" does not exist."
            );

        if (!ul.isMember(requester, GroupServer.ADMIN)) // User must be an admin
            return new Envelope("FAIL").addObject("Requester is not an administrator.");

        if (username.equals(requester)) // Admin can't delete himself
            return new Envelope("FAIL").addObject("Administrator cannot delete himself.");

        ul.deleteUser(username);
        if (GroupServer.DEBUG)
            System.err.println("Deletion successful.");
        return new Envelope("OK");
    }

    /**
     * Create a group.
     * @param groupname Name of the group.
     * @param token The token of the owner-to-be of the group.
     * @return An Envelope indicating success or failure.
     */
    private Envelope createGroup(String groupname, UserToken token)
    {
        String requester = token.getSubject();

        if (GroupServer.DEBUG)
            System.err.printf("Attempting to create group \"%s\".%n", groupname);

        assert requester != null : "Got token with null subject";

        if (!token.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(requester))
            return new Envelope("FAIL").addObject("Requester \"" + requester + "\" does not exist.");

        if (ul.groupExists(groupname))
            return new Envelope("FAIL").addObject(
                "Cannot create group \"" + groupname + "\", group already exists."
            );

        ul.createGroup(groupname, requester);
        if (GroupServer.DEBUG)
            System.err.printf("Successfully created group \"%s\".%n", groupname);
        return new Envelope("OK");
    }

    /**
     * Deletes a group from this server.
     * @param groupname The name of the group to delete.
     * @param yourToken The token of either an administrator or the owner of the
     * group specified by {@code groupname}.
     * @return Returns true iff all of the following are true:
     * <ul>
     *   <li>The group exists</li>
     *   <li>The user identified by the token has sufficient permissions to
     *   delete the group (owner of the group or an administrator).</li>
     *   <li>The group was successfully deleted.</li>
     * </ul>
     */
    private Envelope deleteGroup(String groupname, UserToken yourToken)
    {
        String requester = yourToken.getSubject();
        assert groupname != null : "Got null groupname";
        assert yourToken != null : "Got null token";

        if (groupname.equals(GroupServer.ADMIN))
            return new Envelope("FAIL").addObject(
                "Cannot delete special group " + GroupServer.ADMIN
            );

        if (GroupServer.DEBUG)
            System.err.println("Attempting to delete group \"" + groupname + "\"");

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(requester))
            return new Envelope("FAIL").addObject("Requester \"" + requester + "\" does not exist.");

        if (!ul.groupExists(groupname))
            return new Envelope("FAIL").addObject("Group \"" + groupname + "\" does not exist.");

        if (!ul.isMember(requester, GroupServer.ADMIN)
        &&  !ul.isMember(requester, groupname))
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" is not an admin or group owner."
            );

        ul.deleteGroup(groupname);

        if (GroupServer.DEBUG)
            System.err.println("Deleted group successfully.");

        return new Envelope("OK");
    }

    /**
     * Return an Envelope containing the list of members of a group, or a
     * failure message if the requester does not have permission to perform this
     * action.
     */
    private Envelope listMembers(String groupname, UserToken yourToken)
    {
        String requester = yourToken.getSubject();

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(requester)) // Make sure requester exists
            return new Envelope("FAIL").addObject("Requester \"" + requester + "\" does not exist.");

        if (!ul.groupExists(groupname))
            return new Envelope("FAIL").addObject("Group \"" + groupname + "\" does not exist.");

        if (!ul.isOwner(requester, groupname))
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" is not the owner of this group."
            );

        if (!ul.groupExists(groupname))
            return new Envelope("FAIL").addObject("Group \"" + groupname + "\" does not exist.");

        List<String> membersList = ul.getGroupMembers(groupname);
        return new Envelope("OK").addObject(membersList);
    }

    /**
     * Adds the user {@code username} to the group {@code groupname}, if {@code
     * yourToken} has the appropriate permission to do so.
     * @param username The user.
     * @param groupname The group.
     * @param yourToken A token with permission for this action.
     **/
    private Envelope addUserToGroup(String username, String groupname, UserToken yourToken)
    {
        String requester = yourToken.getSubject();

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(username)) // User must exist.
            return new Envelope("FAIL").addObject(
                "User \"" + username + "\" does not exist."
            );
        if (!ul.groupExists(groupname)) // Group must exist.
            return new Envelope("FAIL").addObject(
                "Group \"" + groupname + "\" does not exist."
            );
        if (!ul.userExists(requester)) // Requester must exist.
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" does not exist."
            );
        if (!ul.isOwner(requester, groupname)) // Requester must be owner of group.
            return new Envelope("FAIL").addObject(
                "Cannot add user to group not owned by this requester."
            );
        if (ul.isMember(username, groupname))
            return new Envelope("FAIL").addObject(
                "User \"" + username + "\" is already in group \""
                + groupname + "\""
            );

        ul.addGroup(username, groupname);
        return new Envelope("OK");
    }

    /**
     * Removes a user from a group.
     * @param username The user.
     * @param groupname The group.
     * @param yourToken The token of the requester.
     **/
    private Envelope removeUserFromGroup(String username, String groupname, UserToken yourToken)
    {
        String requester = yourToken.getSubject();
        if (GroupServer.DEBUG)
            System.err.printf(
                "Requester \"%s\" is attempting to delete user \"%s\" from group \"%s\"%n",
                requester,
                username,
                groupname
            );

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.userExists(username)) // User must exist.
            return new Envelope("FAIL").addObject(
                "Cannot remove nonexistent user \"" + username + "\" from group."
            );
        if (!ul.groupExists(groupname)) // Group must exist.
            return new Envelope("FAIL").addObject(
                "Cannot remove user \"" + groupname + "\" from nonexistent group."
            );
        if (!ul.userExists(requester)) // Requester must exist.
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" does not exist."
            );
        if (!ul.isOwner(requester, groupname)) // Requester must be owner of group.
            return new Envelope("FAIL").addObject(
                "Cannot remove user from group not owned by this requester."
            );
        if (requester.equals(username))
            return new Envelope("FAIL").addObject("Cannot remove owner from group.");

        ul.removeGroup(username, groupname);

        // since we removed a user from this group
        // generate a new key to encrypt files with
        // going forward
        ul.generateNewGroupKey(groupname);

        if (GroupServer.DEBUG)
            System.err.println("Removed user from group successfully.");

        return new Envelope("OK");
    }

    /**
     * Gets a token for the user identified by {@code username}. If the username
     * is {@code null} or empty, an Envelope with the message "FAIL", a null
     * token (field 0), and an error string (field 1) will be returned.
     * @param username
     * @return An Envelope containing the new token, or an error message.
     */
    private Envelope getToken(String username, byte[] fingerprint)
    {
        Envelope response;
        if (username == null)
            return new Envelope("FAIL").addObject("Could not create token: username is null.");
        if (username.length() == 0)
            return new Envelope("FAIL").addObject("Could not create token: username is empty.");

        if (GroupServer.DEBUG)
            System.err.println("User \"" + username + "\" requested a token.");

        if (fingerprint == null)
        {
            if (GroupServer.DEBUG)
                System.err.println("Creating token for this group server.");
            fingerprint = my_gs.fingerprint;
        }

        if (fingerprint.length != 32)
        {
            String err = "Fingerprint has wrong length, was expecting 32-byte"
                       + " SHA-256 hash.";
            if (GroupServer.DEBUG)
                System.err.println(err);
            return new Envelope("FAIL").addObject(err);
        }

        UserToken yourToken = null;
        if (ul.userExists(username)){
            yourToken = new Token(my_gs.name, username, ul.getUserGroups(username));
            // Generate an RSA signature over the string representation of the token
            yourToken.sign(my_gs.keys.getPrivate(), fingerprint);
        }
        // Issue a new token with server's name, user's name, and user's groups
        return new Envelope("OK").addObject(yourToken);
    }

    /**
     * Gets a key for the group identified by {@code groupname}. If the groupname
     * is {@code null} or empty, or the requester is not a member of the group
     * an Envelope with the message "FAIL", a null
     * key (field 0), and an error string (field 1) will be returned.
     * @param groupname
     * @param yourToken
     * @return An Envelope containing the key, or an error message.
     */
    private Envelope getGroupKey(String groupname, UserToken yourToken)
    {
        String requester = yourToken.getSubject();

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.groupExists(groupname)) // Group must exist.
            return new Envelope("FAIL").addObject(
                "Group \"" + groupname + "\" does not exist."
            );
        if (!ul.userExists(requester)) // Requester must exist.
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" does not exist."
            );
        if (!ul.isMember(requester, groupname)) // Requester must be owner of group.
            return new Envelope("FAIL").addObject(
                "Requester is not a member of this group."
            );

        byte[] key = ul.getGroupKey(groupname);
        return new Envelope("OK").addObject(key).addObject(ul.getGroupKeyIndex(groupname, key));
    }

    private Envelope getGroupKey(String groupname, int index, UserToken yourToken)
    {
        String requester = yourToken.getSubject();

        if (!yourToken.verifySignature(my_gs.keys.getPublic(), my_gs.fingerprint))
            return new Envelope("FAIL-BADTOKEN");

        if (!ul.groupExists(groupname)) // Group must exist.
            return new Envelope("FAIL").addObject(
                "Group \"" + groupname + "\" does not exist."
            );
        if (!ul.userExists(requester)) // Requester must exist.
            return new Envelope("FAIL").addObject(
                "Requester \"" + requester + "\" does not exist."
            );
        if (!ul.isMember(requester, groupname)) // Requester must be owner of group.
            return new Envelope("FAIL").addObject(
                "Requester is not a member of this group."
            );

        byte[] key = ul.getGroupKey(groupname, index);
        return new Envelope("OK").addObject(key);
    }
}
