import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;
import java.util.*;
import java.security.*;

/**
 * This is the class for the group server. There should only ever be one group
 * server running at a time in a single file sharing system. This server is
 * communicated with via the {@linkplain GroupClient group client}. It manages
 * the persistent information in the {@linkplain UserList user list}, which is
 * stored in a file named {@literal UserList.bin}. If the user list does not
 * exist, each group server instance will create a new list and make the user
 * the server administrator. On exit, the server saves the user list to file.
 */
public class GroupServer extends Server {

	public static final int SERVER_PORT = 8765;
	public UserList userList;
	public KeyPair keys;
	public byte[] fingerprint;
	public static final File DEFAULT_PUB = new File("groupPub.rsa");
	public static final File DEFAULT_PRV = new File("groupPrv.rsa");

	/* Enable with -DDEBUG_GROUP_SERVER. I don't know if the value will be
	 * inlined (allowing the optimizer to prune unreachable branches), but if it
	 * becomes a performance problem it can be disabled by simply setting it to
	 * false, allowing the JIT to remove all `if (DEBUG)`s before running the
	 * thread. */
	public static final boolean
	    DEBUG = System.getProperty("DEBUG_GROUP_SERVER") != null
		 || System.getProperty("DEBUG_ALL")          != null;

	/* Administrator group. This variable should be used so the name of the
	group can be changed easily if necessary. */
	public static final String ADMIN = "ADMIN";

	/** Construct a GroupServer with default port and key files. */
	public GroupServer() {
		super(SERVER_PORT, "ALPHA");
		loadKeys(DEFAULT_PUB, DEFAULT_PRV);
	}

	/** Construct a GroupServer with default port and specific key files. */
	public GroupServer(File pubFile, File prvFile) {
		super(SERVER_PORT, "ALPHA");
		loadKeys(pubFile, prvFile);
	}

	/** Construct a GroupServer with specific port and default key files. */
	public GroupServer(int _port) {
		super(_port, "ALPHA");
		loadKeys(DEFAULT_PUB, DEFAULT_PRV);
	}

	/** Construct a GroupServer with specific port and key files. */
	public GroupServer(int _port, File pubFile, File prvFile) {
		super(_port, "ALPHA");
		loadKeys(pubFile, prvFile);
	}

	/**
	 * Load this server's keys from the two files and store them in the class
	 * variable {@code keys}.
	 */
	public void loadKeys(File pubFile, File prvFile) {
		try
		{
			keys = KeyGen.readKeys(pubFile, prvFile);
		}
		catch (FileNotFoundException e)
		{
			System.out.println(
				"Error when reading keys (Maybe you need to run KeyGen): "
				+ e.getMessage()
			);
			System.exit(-1);
		}
		catch (Exception e)
		{
			System.out.println("Error when reading keys: " + e.getMessage());
			System.exit(-1);
		}

		// Load fingerprint for group tokens
		fingerprint = CryptoManager.getDigest().digest(
			keys.getPublic().getEncoded()
		);

		System.out.println("Loaded Group Server's RSA keys");
	}

	public void start() {
		// Overwrote server.start() because if no user file exists, initial admin account needs to be created

		String userFile = "UserList.bin";
		Console console = System.console();
		ObjectInputStream userStream;
		ObjectInputStream groupStream;

		//This runs a thread that saves the lists on program exit
		Runtime runtime = Runtime.getRuntime();
		runtime.addShutdownHook(new ShutDownListener(this));

		//Open user file to get user list
		try
		{
			FileInputStream fis = new FileInputStream(userFile);
			userStream = new ObjectInputStream(fis);
			userList = (UserList)userStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("UserList File Does Not Exist. Creating UserList...");
			System.out.println("No users currently exist. Your account will be the administrator.");
			System.out.print("Enter your username: ");
			String username = console.readLine();
			if (username == null || username.length() == 0)
			{
				System.err.println("No username given, exiting.");
				System.exit(-1);
			}

			String password = new String(
				console.readPassword("Enter your password: ")
			);
			/* Create a new list and create the group ADMIN with the current
			   user as its owner. */
			userList = new UserList();
			userList.addUser(
				username,
				new SRP(username, password, new SecureRandom(), SRP.SALT_SIZE)
			);
			userList.createGroup(ADMIN, username);
			System.out.println("Created new user \"" + username + "\" with administrator privileges.");
			password = null;
		}
		catch(Exception e)
		{
			System.out.println("Error reading from UserList file");
			System.exit(-1);
		}

		//Autosave Daemon. Saves lists every 5 minutes
		AutoSave aSave = new AutoSave(this);
		aSave.setDaemon(true);
		aSave.start();

		if (GroupServer.DEBUG)
			System.out.println("Debug logging is enabled.");

		//This block listens for connections and creates threads on new connections
		System.out.println("Listening on port " + port);
		try
		{

			final ServerSocket serverSock = new ServerSocket(port);

			Socket sock = null;
			GroupThread thread = null;

			while(true)
			{
				sock = serverSock.accept();
				thread = new GroupThread(sock, this);
				thread.start();
			}
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}

	}

}

//This thread saves the user list
class ShutDownListener extends Thread
{
	public GroupServer my_gs;

	public ShutDownListener (GroupServer _gs) {
		my_gs = _gs;
	}

	public void run()
	{
		System.out.println("Shutting down server");
		ObjectOutputStream outStream;
		try
		{
			outStream = new ObjectOutputStream(new FileOutputStream("UserList.bin"));
			if (my_gs.userList == null || my_gs.userList.userCount() == 0)
				System.err.println(
					"Warning: userList is null or contains no users."
					+ " The resulting UserList.bin will likely be invalid."
				);
			outStream.writeObject(my_gs.userList);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}

class AutoSave extends Thread
{
	public GroupServer my_gs;

	public AutoSave (GroupServer _gs) {
		my_gs = _gs;
	}

	public void run()
	{
		do
		{
			try
			{
				Thread.sleep(300000); //Save group and user lists every 5 minutes
				System.out.println("Autosave group and user lists...");
				ObjectOutputStream outStream;
				try
				{
					outStream = new ObjectOutputStream(new FileOutputStream("UserList.bin"));
					if (my_gs.userList == null || my_gs.userList.userCount() == 0)
						System.err.println(
							"Warning: userList is null or contains no users."
							+ " The resulting UserList.bin will likely be invalid."
						);
					outStream.writeObject(my_gs.userList);
				}
				catch(Exception e)
				{
					System.err.println("Error: " + e.getMessage());
					e.printStackTrace(System.err);
				}

			}
			catch(Exception e)
			{
				System.out.println("Autosave Interrupted");
			}
		} while(true);
	}
}
