import java.io.*;
import java.lang.Thread;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * Handles the business of uploading, downloading, and removing files for
 * clients with valid tokens.
 */
public class FileThread extends Thread {
	private final Socket socket;
	private FileServer fs;
	private CryptoManager cm;

	public static final String BAD_VERIFY_ERR =
	    "Error: expired or corrupted token.";

	public FileThread(Socket _socket, FileServer _fs) throws IOException {
		fs = _fs;
		socket = _socket;
		cm = new CryptoManager(
			new ObjectInputStream(socket.getInputStream()),
			new ObjectOutputStream(socket.getOutputStream())
		);
	}

	public void authenticate() throws BadHandshakeException, SendException,
		ReadException, EOFException, GeneralSecurityException {
		if (FileServer.DEBUG)
			System.err.println("Sending key and IV");
		// Generate an IV to use as a nonce
		byte[] iv = cm.generateIV();
		cm.setIV(iv);
		// Send the public key and IV
		cm.sendMessagePlaintext(
			new Envelope(null)
			.addObject(fs.keys.getPublic())
			.addObject(iv)
		);
		if (FileServer.DEBUG)
			System.err.println("Waiting for response...");
		// Receive the encrypted AES key and IV
		Envelope resp = cm.readMessagePlaintext();
		if (FileServer.DEBUG)
			System.err.println("Response received");
		if (!resp.hasObjContents(3))
			throw new BadHandshakeException(
				"Didn't receive AES key, IV, and integrity key"
			);
		byte[] encryptedAESKey = (byte[])resp.getObjContents().get(0);
		byte[] encryptedIV     = (byte[])resp.getObjContents().get(1);
		byte[] encryptedIKey   = (byte[])resp.getObjContents().get(2);
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, fs.keys.getPrivate());
		// Decrypt the AES key using RSA prv key
		byte[] decryptedAESKey = cipher.doFinal(encryptedAESKey);
		byte[] decryptedIV     = cipher.doFinal(encryptedIV);
		byte[] integrityKey    = cipher.doFinal(encryptedIKey);
		if (!Arrays.equals(decryptedIV, iv))
			throw new BadHandshakeException("Decrypted IV differs from IV");
		cm.setKey(decryptedAESKey);
		cm.setIntegrityKey(integrityKey);
		cm.setAuthenticated();
	}

	public void run() {
		boolean proceed = true;
		try {
			System.out.println(
				"*** New connection from " + socket.getInetAddress()
				+ ":" + socket.getPort() + "***"
			);

			Envelope response;

			authenticate();

			while (proceed) {
				Envelope e = cm.readMessage();
				System.out.println("Request received: " + e.getMessage());

				// Handler to list files that this user is allowed to see
				if (e.getMessage().equals("LFILES")) {
					if (e.getObjContents().size() < 1) {
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else if (e.getObjContents().get(0) == null) {
						response = new Envelope("FAIL-BADTOKEN");
					}
					else {
						UserToken yourToken = (UserToken)e.getObjContents().get(0); //Extract token

						// verify signature of token
						if (yourToken.verifySignature(fs.groupPubKey, fs.fingerprint)) {
							List<String> yourGroups = yourToken.getGroups();
							ArrayList<ShareFile> files = fs.fileList.getFiles();
							List<String> allowedFiles = new ArrayList<String>();

							for (int i = 0; i < files.size(); i++) {
								if (yourGroups.contains(files.get(i).getGroup())) {
									String path = files.get(i).getPath();

									if (path.charAt(0) == '/') {
										path = path.substring(1);
									}

									allowedFiles.add(path);
								}
							}

							response = new Envelope("OK");
							response.addObject(allowedFiles); //Add file list
						} else {
							response = new Envelope("FAIL-BADTOKEN");
						}
					}
					cm.sendMessage(response);
				}
				if (e.getMessage().equals("UPLOADF")) {

					if (e.getObjContents().size() < 3) {
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else {
						if (e.getObjContents().get(0) == null) {
							response = new Envelope("FAIL-BADPATH");
						}
						if (e.getObjContents().get(1) == null) {
							response = new Envelope("FAIL-BADGROUP");
						}
						if (e.getObjContents().get(2) == null) {
							response = new Envelope("FAIL-BADTOKEN");
						}
						else {
							String remotePath = (String)e.getObjContents().get(0);
							String group = (String)e.getObjContents().get(1);
							UserToken yourToken = (UserToken)e.getObjContents().get(2); //Extract token
							int keyIndex = (int)e.getObjContents().get(3);
							byte[] iv = (byte[])e.getObjContents().get(4);
							byte[] hash = (byte[])e.getObjContents().get(5);

							if (!yourToken.verifySignature(fs.groupPubKey, fs.fingerprint)) {
								System.out.println(BAD_VERIFY_ERR);
								response = new Envelope("FAIL-BADTOKEN");
							} else if (fs.fileList.checkFile(remotePath)) {
								System.out.printf("Error: file already exists at %s%n", remotePath);
								response = new Envelope("FAIL-FILEEXISTS");
							}
							else if (!yourToken.getGroups().contains(group)) {
								System.out.printf("Error: user missing valid token for group %s%n", group);
								response = new Envelope("FAIL-UNAUTHORIZED");
							}
							else  {
								File file = new File("shared_files/"+remotePath.replace('/', '_'));
								file.createNewFile();
								FileOutputStream fos = new FileOutputStream(file);
								System.out.printf("Successfully created file %s%n", remotePath.replace('/', '_'));

								response = new Envelope("READY");
								cm.sendMessage(response);

								e = cm.readMessage();
								while (e.getMessage().equals("CHUNK")) {
									fos.write((byte[])e.getObjContents().get(0), 0, (Integer)e.getObjContents().get(1));
									response = new Envelope("READY");
									cm.sendMessage(response);
									e = cm.readMessage();
								}

								if (e.getMessage().equals("EOF")) {
									System.out.printf("Transfer successful file %s%n", remotePath);
									fs.fileList.addFile(yourToken.getSubject(), group, remotePath, keyIndex, iv, hash);
									response = new Envelope("OK");
								}
								else {
									System.out.printf("Error reading file %s from client%n", remotePath);
									response = new Envelope("ERROR-TRANSFER");
								}
								fos.close();
							}
						}
					}

					cm.sendMessage(response);
				}
				else if (e.getMessage().equals("DOWNLOADF")) {
					String remotePath = (String)e.getObjContents().get(0);
					Token t = (Token)e.getObjContents().get(1);
					ShareFile sf = fs.fileList.getFile("/"+remotePath);
					if (!t.verifySignature(fs.groupPubKey, fs.fingerprint)) {
						System.out.println(BAD_VERIFY_ERR);
						e = new Envelope("FAIL-BADTOKEN");
						cm.sendMessage(e);
					} else if (sf == null) {
						System.out.printf("Error: File %s doesn't exist%n", remotePath);
						e = new Envelope("ERROR_FILEMISSING");
						cm.sendMessage(e);
					}
					else if (!t.getGroups().contains(sf.getGroup())) {
						System.out.printf("Error user %s doesn't have permission%n", t.getSubject());
						e = new Envelope("ERROR_PERMISSION");
						cm.sendMessage(e);
					}
					else {
						try {
							File f = new File("shared_files/_"+remotePath.replace('/', '_'));
							if (!f.exists()) {
								System.out.printf("Error file %s missing from disk%n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_NOTONDISK");
								cm.sendMessage(e);
							}
							else {
								FileInputStream fis = new FileInputStream(f);

								do {
									byte[] buf = new byte[4096];
									if (!e.getMessage().equals("DOWNLOADF")) {
										System.out.printf("Server error: %s%n", e.getMessage());
										break;
									}
									e = new Envelope("CHUNK");
									int n = fis.read(buf); //can throw an IOException
									if (n > 0) {
										System.out.print(".");
									} else if (n < 0) {
										System.out.println("Read error");

									}

									e.addObject(buf);
									e.addObject(new Integer(n));

									cm.sendMessage(e);

									e = cm.readMessage();
								}
								while (fis.available() > 0);
								fis.close();

								//If server indicates success, return the member list
								if (e.getMessage().equals("DOWNLOADF"))
								{
									e = new Envelope("EOF");
									cm.sendMessage(e);

									e = cm.readMessage();
									if (e.getMessage().compareTo("OK")==0) {
										System.out.printf("File data upload successful%n");
									}
									else {
										System.out.printf("Upload failed: %s%n", e.getMessage());
									}
								}
								else {
									System.out.printf("Upload failed: %s%n", e.getMessage());
								}
							}
						}
						catch (Exception e1)
						{
							System.err.println("Error: " + e.getMessage());
							e1.printStackTrace(System.err);

						}
					}
				}
				else if (e.getMessage().equals("GETFILEINFO")) {
					String remotePath = (String)e.getObjContents().get(0);
					Token t = (Token)e.getObjContents().get(1);
					ShareFile sf = fs.fileList.getFile("/"+remotePath);
					if (!t.verifySignature(fs.groupPubKey, fs.fingerprint)) {
						System.out.println(BAD_VERIFY_ERR);
						e = new Envelope("FAIL-BADTOKEN");
						cm.sendMessage(e);
					} else if (sf == null) {
						System.out.printf("Error: File %s doesn't exist%n", remotePath);
						e = new Envelope("ERROR_FILEMISSING");
						cm.sendMessage(e);
					}
					else if (!t.getGroups().contains(sf.getGroup())) {
						System.out.printf("Error user %s doesn't have permission%n", t.getSubject());
						e = new Envelope("ERROR_PERMISSION");
						cm.sendMessage(e);
					}
					else {
						try {
							e = new Envelope("OK");
							e.addObject(sf.getGroup());
							e.addObject(sf.getKeyIndex());
							e.addObject(sf.getIV());
							e.addObject(sf.getHash());

							cm.sendMessage(e);
						}
						catch (Exception e1)
						{
							System.err.println("Error: " + e.getMessage());
							e1.printStackTrace(System.err);
						}
					}
				}
				else if (e.getMessage().equals("DELETEF")) {
					String remotePath = (String)e.getObjContents().get(0);
					Token t = (Token)e.getObjContents().get(1);
					ShareFile sf = fs.fileList.getFile("/"+remotePath);
					if (!t.verifySignature(fs.groupPubKey, fs.fingerprint)) {
						System.out.println(BAD_VERIFY_ERR);
						e = new Envelope("FAIL-BADTOKEN");
					} else if (sf == null) {
						System.out.printf("Error: File %s doesn't exist%n", remotePath);
						e = new Envelope("ERROR_DOESNTEXIST");
					}
					else if (!t.getGroups().contains(sf.getGroup())) {
						System.out.printf("Error user %s doesn't have permission%n", t.getSubject());
						e = new Envelope("ERROR_PERMISSION");
					}
					else {
						try
						{
							File f = new File("shared_files/"+"_"+remotePath.replace('/', '_'));
							if (!f.exists()) {
								System.out.printf("Error file %s missing from disk%n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_FILEMISSING");
							}
							else if (f.delete()) {
								System.out.printf("File %s deleted from disk%n", "_"+remotePath.replace('/', '_'));
								fs.fileList.removeFile("/"+remotePath);
								e = new Envelope("OK");
							}
							else {
								System.out.printf("Error deleting file %s from disk%n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_DELETE");
							}
						}
						catch (Exception e1) {
							System.err.println("Error: " + e1.getMessage());
							e1.printStackTrace(System.err);
							e = new Envelope(e1.getMessage());
						}
					}

					cm.sendMessage(e);
				}
			}
		}
		catch (EOFException e) {
			System.err.println("Input closed.");
			return;
		}
		catch (SendException e) {
			System.err.println("Error sending message: " + e.getMessage());
			return;
		}
		catch (ReadException e) {
			System.err.println("Error reading message: " + e.getMessage());
			return;
		}
		catch (IOException e) {
			e.printStackTrace(System.err);
			return;
		}
		catch (BadHandshakeException e) {
			System.out.println("Authentication failed: " + e.getMessage());
			return;
		}
		catch (GeneralSecurityException e) {
			System.err.println("Fatal security exception:");
			e.printStackTrace(System.err);
			return;
		}
	}
}
