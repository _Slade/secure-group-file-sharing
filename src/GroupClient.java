import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.agreement.srp.*;
import org.bouncycastle.crypto.digests.SHA256Digest;

/** Implements the GroupClient Interface */
public class GroupClient extends Client implements GroupClientInterface
{
    public static final boolean
        DEBUG = System.getProperty("DEBUG_GROUP_CLIENT") != null
             || System.getProperty("DEBUG_ALL")          != null;
    // Stuff that's the same in GroupThread
    // Includes IO streams, cipher, key, and RNG
    CryptoManager cm;
    public UserToken groupToken;

    public GroupClient()
    {
        super();

        if (DEBUG)
            System.err.println("Debugging messages for client enabled");
    }

    @Override
    public boolean connect(final String server, final int port)
    {
        boolean r = super.connect(server, port);
        cm = new CryptoManager(input, output);
        return r;
    }

    /**
     * Test if authentication succeeded and messages can now be transmitted
     * securely.
     */
    public boolean isAuthenticated()
    {
        return cm.isAuthenticated();
    }

    /**
     * Attempt to authenticate with the server by performing an SRP exchange.
     * @param username The name of the user whose identity is being used.
     * @param password The password of that user.
     * @return True if authentication went through successfully, false if the
     * credentials were wrong or something else went wrong.
     * @throws BadHandshakeException If some necessary state is missing (e.g.
     * the salt), the server sends a malformed message, or something else goes
     * wrong.
     * @throws SendException If a message couldn't be sent.
     * @throws ReadException If a message couldn't be read.
     * @throws EOFException If the input was closed.
     */
    public boolean authenticate(String username, String password)
        throws BadHandshakeException, SendException, ReadException, EOFException
    {
        assert cm.isConnected();
        assert !cm.isAuthenticated();
        assert username != null && password != null : "Got null parameter";
        try
        {
            final SRP6Client client;
            final BigInteger
                A,        // Our half of shared secret
                B,        // Server's half
                K,        // Shared secret
                M1,       // Challenge to us
                solvedM1, // Our solution
                M2;       // Challenge to server

            // First, we send our username
            Envelope m1 = new Envelope(null).addObject(username);
            cm.sendMessagePlaintext(m1);

            // While we're waiting, initialize SRP6Client
            client = new SRP6Client();
            client.init(SRP.group, new SHA256Digest(), cm.getRandom());

            // Server sends back salt
            Envelope m2 = cm.readMessagePlaintext();
            if (!m2.hasObjContents(1))
                throw new BadHandshakeException("Salt missing or empty");
            byte[] salt = (byte[])m2.getObjContents().get(0);
            if (salt == null || salt.length < 1)
                throw new BadHandshakeException("Salt missing or empty");
            // Calculate our half of shared secret
            A = client.generateClientCredentials(
                salt,
                SRP.stringToBytes(username),
                SRP.stringToBytes(password)
            );

            password = null; // Let GC clean up as soon as possible

            // Send A to server
            Envelope m3 = new Envelope(null).addObject(A);
            cm.sendMessagePlaintext(m3);

            // Server sends back B and M1
            Envelope m4 = cm.readMessagePlaintext();
            if (!m4.hasObjContents(1))
                throw new BadHandshakeException("B missing from message 4");
            B = (BigInteger)m4.getObjContents().get(0);
            // Throws CryptoException if B is bad
            client.calculateSecret(B);
            // Send our evidence message
            M1 = client.calculateClientEvidenceMessage();
            Envelope m5 = new Envelope(null).addObject(M1);
            cm.sendMessagePlaintext(m5);
            // If that was good, verify the server's evidence message
            Envelope m6 = cm.readMessagePlaintext();
            if (!m6.hasObjContents(3))
                throw new BadHandshakeException("M2 missing from message 6");
            M2 = (BigInteger)m6.getObjContents().get(0);
            if (!client.verifyServerEvidenceMessage(M2))
                throw new BadHandshakeException("Got bad evidence from server");
            // Set cipher IV
            cm.setIV((byte[])m6.getObjContents().get(1));
            // Calculate session key using shared secret
            cm.initKey(client.calculateSessionKey());

            byte[] encryptedKey = (byte[])m6.getObjContents().get(2);
            cm.setIntegrityKey(cm.decrypt(encryptedKey));
            return true;
        }
        catch (SendException | ReadException e)
        {
            e.printStackTrace(System.err);
            return false;
        }
        catch (CryptoException e)
        {
            if (DEBUG)
                System.err.println(e.getMessage());
            return false;
        }
        catch (EOFException e)
        {
            return false;
        }
    }

    public UserToken getToken(String username)
    {
        if (DEBUG)
            System.err.println("Attempting to get group token from group server");
        return getToken(username, null);
    }

    public UserToken getToken(String username, byte[] fingerprint)
    {
        // We need an encrypted connection to get the token
        if (!cm.isAuthenticated())
        {
            if (DEBUG)
                System.err.println("Not authenticated, returning from getToken()");
            return null;
        }
        try
        {
            UserToken token = null;
            Envelope message = null, response = null;

            //Tell the server to return a token.
            message = new Envelope("GET");
            message.addObject(username); //Add user name string
            /* Fingerprint of the server, used to request a token for a file
            server */
            if (fingerprint != null)
                message.addObject(fingerprint);
            cm.sendMessage(message);

            //Get the response from the server
            response = cm.readMessage();

            //Successful response
            if(response.getMessage().equals("OK"))
            {
                //If there is a token in the Envelope, return it
                List<Object> temp = null;
                temp = response.getObjContents();

                if(temp.size() == 1)
                {
                    token = (UserToken)temp.get(0);
                    return token;
                }
            }
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return null;
    }

    public boolean createUser(String username, String pwd, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to create a user
            message = new Envelope("CUSER");
            message.addObject(username); //Add user name string
            message.addObject(pwd); //Add user's password
            message.addObject(token); //Add the requester's token
            cm.sendMessage(message);

            response = cm.readMessage();

            //If server indicates success, return true
            if(response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));

        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    public boolean deleteUser(String username, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;

            //Tell the server to delete a user
            message = new Envelope("DUSER");
            message.addObject(username); //Add user name
            message.addObject(token);  //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();

            //If server indicates success, return true
            if(response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    public boolean createGroup(String groupname, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to create a group
            message = new Envelope("CGROUP");
            message.addObject(groupname); //Add the group name string
            message.addObject(token); //Add the requester's token
            cm.sendMessage(message);

            response = cm.readMessage();

            //If server indicates success, return true
            if (response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    public boolean deleteGroup(String groupname, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to delete a group
            message = new Envelope("DGROUP");
            message.addObject(groupname); //Add group name string
            message.addObject(token); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();
            //If server indicates success, return true
            if(response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));

        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<String> listMembers(String group, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to return the member list
            message = new Envelope("LMEMBERS");
            message.addObject(group); //Add group name string
            message.addObject(token); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();

            //If server indicates success, return the member list
            if(response.getMessage().equals("OK"))
                return (List<String>)response.getObjContents().get(0); //This cast creates compiler warnings. Sorry.
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return null;
    }

    public boolean addUserToGroup(String username, String groupname, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to add a user to the group
            message = new Envelope("AUSERTOGROUP");
            message.addObject(username); //Add user name string
            message.addObject(groupname); //Add group name string
            message.addObject(token); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();
            //If server indicates success, return true
            if(response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    public boolean deleteUserFromGroup(String username, String groupname, UserToken token)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to remove a user from the group
            message = new Envelope("RUSERFROMGROUP");
            message.addObject(username); //Add user name string
            message.addObject(groupname); //Add group name string
            message.addObject(token); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();
            //If server indicates success, return true
            if(response.getMessage().equals("OK"))
                return true;
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return false;
    }

    public int getGroupKey(byte[] keyBytes, String groupname)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to remove a user from the group
            message = new Envelope("GGROUPKEY");
            message.addObject(groupname); //Add group name string
            message.addObject(groupToken); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();
            //If server indicates success, get the key
            if(response.getMessage().equals("OK")) {
                byte[] key = (byte[])response.getObjContents().get(0);

                System.arraycopy(key, 0, keyBytes, 0, key.length);

                return (int)response.getObjContents().get(1);
            }
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return -1;
    }

    public byte[] getGroupKey(String groupname, int keyIndex)
    {
        try
        {
            Envelope message = null, response = null;
            //Tell the server to remove a user from the group
            message = new Envelope("GGROUPKEYI");
            message.addObject(groupname); //Add group name string
            message.addObject(keyIndex); //Add group keys index
            message.addObject(groupToken); //Add requester's token
            cm.sendMessage(message);

            response = cm.readMessage();
            //If server indicates success, get the key
            if(response.getMessage().equals("OK"))
                return (byte[])response.getObjContents().get(0);
            else
                System.out.println(getErrorMessage(response));
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        return null;
    }
}
