import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.*;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

// Exceptions
import java.io.EOFException;
import java.io.IOException;
import java.lang.ClassNotFoundException;

/**
 * This class encapsulates the commonality between a client and server.
 * Both client and server have input/output object streams, a cipher instance
 * for encrypting/decrypting, a secure random number generator, a secret key,
 * and some static parameters used between the two.
 *
 * By putting those common aspects in this class, certain operations such as
 * encrypting and decrypting or sending and receiving messages can be
 * implemented in a single place.
 */
public class CryptoManager
{
    static
    {
        Provider p = new BouncyCastleProvider();
        Security.addProvider(p);
    }

    /** The output stream through which messages are sent. */
    private ObjectOutputStream output        = null;
    /** The input stream through which messages are read. */
    private ObjectInputStream  input         = null;
    /** The cipher by which messages are encrypted and decrypted. */
    private Cipher             cipher        = null;
    /** The key used by the cipher. */
    private SecretKey          key           = null;
    /** The random number generator used by the cipher. */
    private SecureRandom       rng           = null;
    /** The IV used for this cipher. */
    private IvParameterSpec    iv            = null;
    /** A boolean indicating whether authentication (establishment of the key)
     * has been performed. */
    private boolean            authenticated = false;
    /**
     * The sequence number of (encrypted) messages sent/received. If either of
     * our handshake protocols turn out to be vulnerable to reordering attacks,
     * this will have to be rethought in order to include unencrypted messages.
     */
    private int sequenceNumber = -1;

    private Mac mac = null;

    public static final int
        CIPHER_KEY_SIZE = 32,
        IV_SIZE         = 16,
        MAC_KEY_SIZE    = 16;
    public static final String
        PROVIDER        = "BC",
        CIPHER_TYPE     = "AES/OFB/PKCS5PADDING",
        CIPHER_KEY_TYPE = "AES",
        MAC_TYPE        = "SipHash-2-4",
        MAC_KEY_TYPE    = "SipHash",
        DIGEST_TYPE     = "SHA-256";
    private static final String
        ENOOS = "Tried to send message with uninitialized output stream",
        ENOIS = "Tried to read message with uninitialized input stream",
        ENOC  = "Tried to encrypt or decrypt message with no cipher";

    public CryptoManager()
    {
        try
        {
            cipher = Cipher.getInstance(CIPHER_TYPE, PROVIDER);
            mac    = Mac.getInstance(MAC_TYPE, PROVIDER);
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        rng = new SecureRandom();
    }

    public CryptoManager(ObjectInputStream i, ObjectOutputStream o)
    {
        this();
        input  = i;
        output = o;
    }

    /**
     * Send a message {@code m} in plaintext. This method requires the output
     * stream {@code output} to have been initialized. This should be used
     * sparingly, e.g. for establishing a secure connection. Once a secure
     * connection is establishted, use the non-plaintext versions instead.
     * @param m The message to send.
     * @return A boolean indicating success (true) or failure (false).
     * @throws SendException If something went wrong when sending.
     */
    public boolean sendMessagePlaintext(Envelope m) throws SendException
    {
        assert output != null : ENOOS;
        try
        {
            output.writeObject(m);
        }
        catch (IOException e)
        {
            throw new SendException(e);
        }
        return true;
    }

    /**
     * Send an encrypted message {@code m}. This requires the output stream
     * {@code output}, the cipher {@code cipher}, and the key {@code key} to
     * have been initialized.
     * @param m the message to send
     * @return A boolean indicating success (true) or failure (false)
     * @throws SendException if something went wrong when sending
     */
    public boolean sendMessage(Envelope m) throws SendException
    {
        assert output != null : ENOOS;
        assert key != null : ENOC;
        if (!authenticated)
            throw new SendException("Not authenticated");

        m.setSequenceNumber(++sequenceNumber);
        try
        {
            byte[][] wrapper = new byte[2][];
            wrapper[0] = encrypt(Envelope.toBytes(m)); // Message
            wrapper[1] = mac.doFinal(wrapper[0]);      // MAC
            output.writeObject(wrapper);
            return true;
        }
        catch (IOException e)
        {
            throw new SendException(e);
        }
    }

    /**
     * Reads a plaintext message.
     * @return The Envelope message, or {@code null} if failed.
     * @throws ReadException If a problem occurred when reading the message.
     * @throws EOFException If {@code input} reached EOF.
     */
    public Envelope readMessagePlaintext() throws ReadException, EOFException
    {
        assert input != null : ENOIS;
        try
        {
            return (Envelope)input.readObject();
        }
        catch (EOFException e)
        {
            throw e;
        }
        catch (IOException | ClassNotFoundException e)
        {
            throw new ReadException(e);
        }
    }

    /**
     * Reads an encrypted message.
     * @return The Envelope in plaintext, or null if failed.
     * @throws ReadException If the message could not be decrypted or if
     * something else went wrong.
     * @throws EOFException If {@code input} reached EOF.
     */
    public Envelope readMessage() throws ReadException, EOFException
    {
        assert input != null : ENOIS;
        assert cipher != null && key != null : ENOC;
        if (!authenticated || key == null)
            throw new ReadException(
                "Cannot decrypt prior to authentication."
                + " If you really wanted to read an unencrypted message, use"
                + " the plaintext version of this method instead."
            );

        assert cipher != null : "Tried to decrypt with null cipher";
        try
        {
            byte[][] wrapper = (byte[][])input.readObject();
            byte[] encrypted = wrapper[0];
            byte[] mmac      = wrapper[1];
            if (!Arrays.equals(mmac, mac.doFinal(encrypted)))
                throw new ReadException("Corrupted message or bad MAC");
            byte[] decrypted = decrypt(encrypted);
            Envelope e = Envelope.fromBytes(decrypted);
            if (sequenceNumber < 0 && e.getSequenceNumber() == 0)
                sequenceNumber = 0; // Just received first message
            else if (sequenceNumber + 1 == e.getSequenceNumber())
                ++sequenceNumber;
            else
                throw new ReadException(
                    "Message " + e.getSequenceNumber() + " out of sequence"
                );
            return e;
        }
        catch (EOFException e)
        {
            throw e;
        }
        catch (IOException | ClassNotFoundException e)
        {
            throw new ReadException(e);
        }
    }

    public byte[] encrypt(byte[] plaintext)
    {
        assert cipher != null && key != null && iv != null;
        byte[] ciphertext = null;
        try
        {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            ciphertext = cipher.doFinal(plaintext);
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return ciphertext;
    }

    public byte[] decrypt(byte[] ciphertext)
    {
        assert cipher != null && key != null && iv != null;
        byte[] plaintext = null;
        try
        {
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            plaintext = cipher.doFinal(ciphertext);
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return plaintext;
    }

    public byte[] generateKey()
    {
        byte[] keyBytes = new byte[CIPHER_KEY_SIZE];
        rng.nextBytes(keyBytes);
        return keyBytes;
    }

    public void setKey(byte[] keyBytes)
    {
        key = new SecretKeySpec(keyBytes, 0, CIPHER_KEY_SIZE, CIPHER_KEY_TYPE);
    }

    public byte[] generateIV()
    {
        byte[] tmpIv = new byte[IV_SIZE];
        rng.nextBytes(tmpIv);
        return tmpIv;
    }

    public void setIV(byte[] _iv)
    {
        assert _iv.length == IV_SIZE : "Got IV with size " + _iv.length;
        iv = new IvParameterSpec(_iv);
    }

    public void setAuthenticated()
    {
        authenticated = true;
    }

    public void setNotAuthenticated()
    {
        authenticated = false;
    }

    public boolean isAuthenticated()
    {
        return authenticated;
    }

    /** Generates the integrity key for the MAC. */
    public byte[] generateIntegrityKey()
    {
        byte[] key = new byte[16];
        rng.nextBytes(key);
        return key;
    }

    /** Sets the integrity key used by the MAC. */
    public void setIntegrityKey(byte[] key)
    {
        try
        {
            mac.init(new SecretKeySpec(key, MAC_KEY_TYPE));
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
    }

    /**
     * Initializes this object's key using the session key returned by
     * SRP6Client/Server.calculateSessionKey(). Additionally, set {@code
     * authenticated} to reflect success or failure.
     * @param sessionKey
     * @return True iff initialization was successful.
     */
    public boolean initKey(BigInteger sessionKey)
    {
        byte[] k1 = sessionKey.toByteArray();
        // Use SHA256 digest as cipher key
        setKey(getDigest().digest(k1));
        return authenticated = true;
    }

    public void setInput(ObjectInputStream inputStream)
    {
        this.input = inputStream;
    }

    public void setOutput(ObjectOutputStream outputStream)
    {
        this.output = outputStream;
    }

    public void setRandom(SecureRandom rng)
    {
        this.rng = rng;
    }

    public SecureRandom getRandom()
    {
        return rng;
    }

    public boolean isConnected()
    {
        return input != null && output != null;
    }

    /** Utility method for creating SHA-256 MessageDigests. */
    public static MessageDigest getDigest()
    {
        MessageDigest digest;
        try
        {
            return MessageDigest.getInstance(DIGEST_TYPE);
        }
        catch (NoSuchAlgorithmException e)
        {
            System.err.println(
                "Could not make digest of type " + DIGEST_TYPE + ":"
            );
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return null;
    }
}
