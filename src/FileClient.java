import java.io.Console;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.*;
import org.bouncycastle.util.encoders.Hex;

/**
 * Provides all the client functionality regarding the file server
 */
public class FileClient extends Client implements FileClientInterface {

	public static final boolean
		DEBUG = System.getProperty("DEBUG_FILE_CLIENT") != null
		     || System.getProperty("DEBUG_ALL")         != null;
	CryptoManager cm;
	GroupClient gc;

	public FileClient() {
		super();
	}

	public FileClient(GroupClient groupClient) {
		super();
		gc = groupClient;
	}

	@Override
	public boolean connect(final String server, final int port) {
		boolean r = super.connect(server, port);
		cm = new CryptoManager(input, output);
		return r;
	}

	/**
	 * Authenticates the file server and establishes a secure connection.
	 * @param user The username to log in as.
	 * @return The fingerprint of the file server, used to get a token from the
	 * group server that corresponds to this particular file server.
	 */
	@SuppressWarnings("unchecked")
	public byte[] authenticate(UserToken groupToken) throws BadHandshakeException,
		SendException, ReadException, EOFException, GeneralSecurityException {
		final String user = groupToken.getSubject();
		boolean auth      = false;
		Console console   = System.console();
		Key pubKey        = null;
		byte[] pk         = null;
		byte[] iv         = null;
		Cipher cipher;

		Envelope resp = cm.readMessagePlaintext();
		if (!resp.hasObjContents(2)){ // Public key and IV
			throw new BadHandshakeException("Missing public key or IV");
		}
		pk     = ((Key)resp.getObjContents().get(0)).getEncoded();
		pubKey = (Key)resp.getObjContents().get(0);
		iv     = (byte[])resp.getObjContents().get(1);

		byte[] fingerprint = CryptoManager.getDigest().digest(pk);
		String hexprint    = Hex.toHexString(fingerprint);

		Map<String, ArrayList<String>> fileServerList;
		//Open file server list
		try (FileInputStream fis = new FileInputStream("FileServerList.bin");
			 ObjectInputStream fileStream = new ObjectInputStream(fis)) {
			fileServerList = (HashMap<String, ArrayList<String>>)fileStream.readObject();
		}
		catch(FileNotFoundException e){
			System.out.println("FileServerList does not exist. Creating FileServerList...");
			fileServerList = new HashMap<String, ArrayList<String>>();
		}
		catch(IOException | ClassNotFoundException e){
			System.err.println(
				"Error reading from FileServerList file: " + e.getMessage()
			);
			fileServerList = new HashMap<String, ArrayList<String>>();
		}

		ArrayList<String> trustedServers = fileServerList.get(user);

		if(trustedServers == null){
			trustedServers = new ArrayList<String>();
		}

		while (true) {
			if(trustedServers.contains(hexprint)){
				auth = true;
				cm.setAuthenticated();
				System.out.println("This server is trusted.");
				break;
			} else {
				System.out.println(
					"Do you trust this public key (SHA-256 fingerprint)? (y/n): "
					+ "\n\t" + Hex.toHexString(fingerprint)
				);
				String ans = console.readLine();
				if (ans.equalsIgnoreCase("y")) {
					auth = true;
					cm.setAuthenticated();
					trustedServers.add(hexprint);
					break;
				} else if (ans.equalsIgnoreCase("n")) {
					throw new BadHandshakeException("Server not trusted");
				} else {
					System.out.println("Unknown command.");
				}
			}
		}

		fileServerList.put(user, trustedServers);

		try (ObjectOutputStream outStream = new ObjectOutputStream(
				new FileOutputStream("FileServerList.bin"))) {
			outStream.writeObject(fileServerList);
		}
		catch(IOException e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}

		byte[] keyBytes = cm.generateKey(); // Generate AES key
		cm.setKey(keyBytes);
		cm.setIV(iv);
		byte[] integrityKey = cm.generateIntegrityKey();
		cm.setIntegrityKey(integrityKey);

		cipher = Cipher.getInstance("RSA");
		if (pubKey != null) {
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			// Encrypt AES key with RSA public key
			byte[] encryptedKey  = cipher.doFinal(keyBytes);
			byte[] encryptedIV   = cipher.doFinal(iv);
			byte[] encryptedIKey = cipher.doFinal(integrityKey);
			cm.sendMessagePlaintext(
				new Envelope(null)
				.addObject(encryptedKey)
				.addObject(encryptedIV)
				.addObject(encryptedIKey)
			);
		}

		return fingerprint;
	}

	public boolean delete(final String filename, final UserToken token)
		throws SendException, ReadException, EOFException {
		String remotePath;
		if (filename.charAt(0) == '/') {
			remotePath = filename.substring(1);
		}
		else {
			remotePath = filename;
		}
		Envelope env = new Envelope("DELETEF"); //Success
		env.addObject(remotePath);
		env.addObject(token);
		cm.sendMessage(env);
		env = cm.readMessage();

		if (env.getMessage().compareTo("OK")==0) {
			System.out.printf("File %s deleted successfully%n", filename);
		}
		else {
			System.out.printf("Error deleting file %s (%s)%n", filename, getErrorMessage(env));
			return false;
		}

		return true;
	}

	public boolean download(
		String sourceFile, String destFile, final UserToken token
	) throws SendException, ReadException, EOFException {
		if (sourceFile.charAt(0) == '/') {
			sourceFile = sourceFile.substring(1);
		}

		File file = new File(destFile);
		try {
			if (!file.exists()) {
				file.createNewFile();
				FileOutputStream fos = new FileOutputStream(file);

				Envelope env = new Envelope("GETFILEINFO");
				env.addObject(sourceFile);
				env.addObject(token);
				cm.sendMessage(env);

				String groupname = "";
				int keyIndex = -1;
				byte[] _iv = new byte[CryptoManager.IV_SIZE];
				byte[] storedHash = new byte[256];

				env = cm.readMessage();

				if(env.getMessage().equals("OK")) {
					groupname = (String)env.getObjContents().get(0);
					keyIndex = (int)env.getObjContents().get(1);
					_iv = (byte[])env.getObjContents().get(2);
					storedHash = (byte[])env.getObjContents().get(3);
				} else {
					System.out.println("Error getting file info: " + destFile);
					return false;
				}

				byte[] keyBytes = gc.getGroupKey(groupname, keyIndex);
				SecretKey groupKey = new SecretKeySpec(keyBytes, 0, CryptoManager.CIPHER_KEY_SIZE, CryptoManager.CIPHER_KEY_TYPE);
				IvParameterSpec iv = new IvParameterSpec(_iv);

				Cipher cipher = Cipher.getInstance(CryptoManager.CIPHER_TYPE, CryptoManager.PROVIDER);
				cipher.init(Cipher.DECRYPT_MODE, groupKey, iv);

				CipherOutputStream cos = new CipherOutputStream(fos, cipher);

				env = new Envelope("DOWNLOADF"); //Success
				env.addObject(sourceFile);
				env.addObject(token);
				cm.sendMessage(env);

				env = cm.readMessage();

				while (env.getMessage().equals("CHUNK")) {
					cos.write(
						(byte[])env.getObjContents().get(0),
						0,
						(Integer)env.getObjContents().get(1)
					);
					System.out.print(".");
					env = new Envelope("DOWNLOADF"); //Success
					cm.sendMessage(env);
					env = cm.readMessage();
				}
				cos.close();

				if (env.getMessage().equals("EOF")) {
					cos.close();
					System.out.printf(
						"%nTransfer successful file %s%n", sourceFile
					);
					env = new Envelope("OK"); //Success
					cm.sendMessage(env);

					FileInputStream hashStream = new FileInputStream(file);
					byte[] fileBytes = new byte[(int)file.length()];
					hashStream.read(fileBytes);
					hashStream.close();
					byte[] hash = CryptoManager.getDigest().digest(fileBytes);

					if(!Arrays.equals(storedHash, hash)){
						System.out.printf("File integrity check failed, file deleted: %s%n", sourceFile);
						file.delete();
						return false;
					}
				}
				else {
					System.out.printf(
						"Error reading file %s (%s)%n",
						sourceFile,
						getErrorMessage(env)
					);
					file.delete();
					return false;
				}
			}
			else {
				System.out.println("Error creating file " + destFile);
				return false;
			}
		} catch (IOException e) {
			System.out.println("Error creating file " + destFile);
			return false;
		} catch (Exception e) {
			System.err.println("Error:");
			e.printStackTrace(System.err);
			return false;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public List<String> listFiles(final UserToken token)
		throws SendException, ReadException, EOFException {
		try {
			Envelope message = null, e = null;
			//Tell the server to return the member list
			message = new Envelope("LFILES");
			message.addObject(token); //Add requester's token
			cm.sendMessage(message);

			e = cm.readMessage();

			//If server indicates success, return the member list
			if (e.getMessage().equals("OK")) {
				//This cast creates compiler warnings. Sorry.
				return (List<String>)e.getObjContents().get(0);
				/* Note: This is because List<String> is a parameterized type,
				 * meaning that the compiler can't verify at runtime whether
				 * its contents are valid String types due to type
				 * nullification. It ends up being semantically equivalent to
				 * a List<> as a result. Beware of bugs not caught by the
				 * typechecker as a result of this. -- Chris
				 */
			}
			else
				System.out.println(getErrorMessage(e));

			return null;
		}
		catch (Exception e) {
			System.err.println("Error:");
			e.printStackTrace(System.err);
			return null;
		}
	}

	public boolean upload(
		String sourceFile, String destFile,
		final String group, final UserToken token
	) throws SendException, ReadException, EOFException {

		if (destFile.charAt(0) != '/') {
			destFile = "/" + destFile;
		}

		byte[] keyBytes = new byte[CryptoManager.CIPHER_KEY_SIZE];
		int keyIndex = gc.getGroupKey(keyBytes, group);
		byte[] _iv = cm.generateIV();
		SecretKey groupKey = new SecretKeySpec(
			keyBytes,
			0,
			CryptoManager.CIPHER_KEY_SIZE,
			CryptoManager.CIPHER_KEY_TYPE
		);
		IvParameterSpec iv = new IvParameterSpec(_iv);
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(
				CryptoManager.CIPHER_TYPE,
				CryptoManager.PROVIDER
			);
			cipher.init(Cipher.ENCRYPT_MODE, groupKey, iv);
		} catch (GeneralSecurityException e) {
			e.printStackTrace(System.err);
			System.exit(-1);
		}

		try {
			File hashFile = new File(sourceFile);
			FileInputStream hashStream = new FileInputStream(hashFile);
			byte[] fileBytes = new byte[(int)hashFile.length()];
			hashStream.read(fileBytes);
			hashStream.close();
			byte[] hash = CryptoManager.getDigest().digest(fileBytes);


			FileInputStream fis = new FileInputStream(sourceFile);
			CipherInputStream cis = new CipherInputStream(fis, cipher);
			Envelope message = null, env = null;

			message = new Envelope("UPLOADF");
			message.addObject(destFile);
			message.addObject(group);
			message.addObject(token); //Add requester's token
			message.addObject(keyIndex); //Add key index
			message.addObject(_iv); //Add IV
			message.addObject(hash);
			cm.sendMessage(message);

			env = cm.readMessage();

			//If server indicates success, return the member list
			if (env.getMessage().equals("READY")) {
				System.out.println("Meta data upload successful.");

			}
			else {
				System.out.println("Upload failed: " + getErrorMessage(env));
				return false;
			}

			int n = 0;
			do {
				byte[] buf = new byte[4096];
				if (!env.getMessage().equals("READY")) {
					System.out.println("Server error: " + getErrorMessage(env));
					return false;
				}
				message = new Envelope("CHUNK");
				n = cis.read(buf); //can throw an IOException
				if (n > 0) {
					System.out.print(".");
				} else if (n < 0) {
					break;
				}

				message.addObject(buf);
				message.addObject(new Integer(n));

				cm.sendMessage(message);

				env = cm.readMessage();
			}
			while (n != -1);

			//If server indicates success, return the member list
			if (env.getMessage().equals("READY")) {

				message = new Envelope("EOF");
				cm.sendMessage(message);

				env = cm.readMessage();
				if (env.getMessage().equals("OK")) {
					System.out.printf("%nFile data upload successful%n");
				}
				else {
					System.out.printf("%nUpload failed: %s%n", getErrorMessage(env));
					return false;
				}
			}
			else {
				System.out.println("Upload failed: " + getErrorMessage(env));
				return false;
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		} catch (Exception e) {
			System.err.println("Error:");
			e.printStackTrace(System.err);
			return false;
		}
		return true;
	}
}
