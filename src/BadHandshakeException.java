public class BadHandshakeException extends Exception
{
    static final long serialVersionUID = 3046643978957966180L;

    public BadHandshakeException() { }

    public BadHandshakeException(String message)
    {
        super(message);
    }

    public BadHandshakeException(Throwable cause)
    {
        super(cause);
    }

    public BadHandshakeException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
