import java.io.EOFException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Scanner;

public class FileApp implements Runnable {
    String server;
    int port;
    FileClient fc;
    Scanner input;
    int menuChoice;
    UserToken userToken;


    public FileApp(String server, int port, UserToken userToken, FileClient fc){
        this.server = server;
        this.port = port;
        this.userToken = userToken;
        this.fc = fc;
    }

    public void run() {
        input = new Scanner(System.in);

        assert fc.isConnected()
            : "ClientApp gave FileApp a disconnected FileClient";

        boolean keepRunning = true;
        while (keepRunning) {
            System.out.println("Please select a number from the menu: " +
                    "\n\t1. Upload a file" +
                    "\n\t2. Download a file" +
                    "\n\t3. Delete a file" +
                    "\n\t4. List files you have access to" +
                    "\n\t5. Exit");

            menuChoice = input.nextInt();
            input.nextLine(); // Eats the new line, necessary for proper input later on
            try {
                keepRunning = doChoice(menuChoice);
            } catch (ReadException e) {
                System.err.println("Unable to read message: " + e.getMessage());
                return;
            } catch (SendException e) {
                System.err.println("Unable to send message: " + e.getMessage());
                return;
            } catch (EOFException e) {
                System.out.println("Connection closed.");
                return;
            }
        }
    }

    public boolean doChoice(int menuChoice)
        throws ReadException, SendException, EOFException {
        switch (menuChoice) {
            case 1: {    //Upload a file
                System.out.println("Enter the path to the file you want to upload: ");
                String sourceFile = input.nextLine();

                System.out.println("What do you want to name the file on the server?: ");
                String destFile = input.nextLine();

                System.out.println("What group should have access to this file?: ");
                String group = input.nextLine();

                try {
                    if (fc.upload(sourceFile, destFile, group, userToken)) {
                        System.out.println("File '" + destFile + "' successfully uploaded.");
                    } else {
                        System.err.println("Error. Could not upload file.");
                    }
                } catch (SendException | ReadException e) {
                    System.err.println("Unable to send or receive messages.");
                }
                break;
            }
            case 2: {   //Download a file
                System.out.println("Enter the file name you want to download: ");
                String sourceFile = input.nextLine();

                System.out.println("What do you want to name the file locally?: ");
                String destFile = input.nextLine();

                if (fc.download(sourceFile, destFile, userToken)) {
                    System.out.println("File '" + destFile + "' successfully downloaded.");
                } else {
                    System.err.println("Error. Could not download file.");
                }
                break;
            }
            case 3: {   //Delete a file
                System.out.println("Enter the file name you want to delete: ");
                String deleteFile = input.nextLine();

                if (fc.delete(deleteFile, userToken)) {
                    System.out.println("File '" + deleteFile + "' successfully deleted.");
                } else {
                    System.out.println("Error. Could not delete file.");
                }
                break;
            }
            case 4: {   //List files the user has access to
                List<String> userFiles = fc.listFiles(userToken);

                // If userFiles == null, FileClient prints the error message
                if (userFiles == null) {
                    break;
                }

                if (userFiles.size() == 0) {
                    System.out.println("You have access to no files on this server.");
                } else {
                    for (String fileName : userFiles) {
                        System.out.println("\t" + fileName);
                    }
                }
                break;
            }
            case 5: { //Exit
                return false;
            }
            default:
                System.out.println("Unknown Command.");
            break;
        }
        return true;
    }
}
