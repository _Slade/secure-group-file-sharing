import java.io.Console;
import java.io.EOFException;
import java.security.GeneralSecurityException;

public class ClientApp {

    public static void main(String[] args){

        Console input = System.console();
        if (input == null){
            System.err.println("Error: Could not find this VM's console.");
            return;
        }

        String username;
        String server = "localhost";    //Default server
        int port = 8765;                //Default port
        UserToken groupToken;
        int menuChoice;

        if(args.length >= 2){
            server = args[0];
            try {
                port = Integer.parseInt(args[1]);
            }
            catch (NumberFormatException e) {
                System.err.println("Error: Could not understand port \"" + args[1] + "\"");
                return; // Exit program
            }
        }

        final GroupClient gc;
        final FileClient fc;

        System.out.println("Hello. Enter 1 to login, or 2 to exit");
        String in = input.readLine();
        if (in == null) // EOT
            return;

        int userChoice;
        try {
            userChoice = Integer.parseInt(in);
        } catch(Exception e) {
            userChoice = 0;
        }

        if(userChoice == 1){    //User wants to login.
            gc = new GroupClient();
            if (!gc.connect(server, port)) {
                System.out.println("Failed to connect. Exiting.");
                return;
            }

            System.out.print("Enter your username: ");
            username = input.readLine();
            if(username == null){ // EOT
                return;
            }
            String password = new String(
                input.readPassword("Enter your password: ")
            );
            if(password == null){
                return;
            }
            boolean isAuthenticated = false;
            try {
                gc.authenticate(username, password);
            }
            catch (BadHandshakeException e) {
                System.out.println("Authentication failed: " + e.getMessage());
                return;
            }
            catch (SendException | ReadException e) {
                System.err.println(
                    "Error sending or reading messages: " + e.getMessage()
                );
                return;
            }
            catch (EOFException e) {
                System.out.println("Input closed.");
                return;
            }
            finally {
                password = null; // Let GC clean up as soon as possible
            }

            if(!gc.isAuthenticated()){
                System.out.println(
                    "Username or password was incorrect. Exiting."
                );
                return;
            }

            assert gc.isConnected() : "Group Client somehow not connected";

            groupToken = gc.getToken(username); //Get group token
            gc.groupToken = groupToken;

            if(groupToken != null){
                System.out.println("Select a number from the menu below:" +
                        "\n\t1. Login to Group Server." +
                        "\n\t2. Login to File Server.");

                try {
                    String tmp = input.readLine();
                    if (tmp == null)
                        return;
                    menuChoice = Integer.parseInt(tmp);
                } catch (NumberFormatException e) {
                    System.out.println("Could not parse your input.");
                    return;
                }

                if(menuChoice == 1){    //Run Group Client
                    System.out.println("Connecting to Group Server...");
                    GroupApp ga = new GroupApp(server, port, groupToken, gc);
                    ga.run();
                }

                else if(menuChoice == 2){ //Run File Server
                    String fileServer;
                    int filePort;

                    System.out.print("Enter the address of the File Server: ");
                    fileServer = input.readLine();
                    if (fileServer == null)
                        return;

                    System.out.print("Enter the port to connect on: ");
                    try {
                        filePort = Integer.parseInt(input.readLine());
                    } catch (NumberFormatException e) {
                        System.out.println(
                            "Could not understand input. Exiting."
                        );
                        return;
                    }

                    System.out.println("Connecting to File Server...");

                    fc = new FileClient(gc);
                    fc.connect(fileServer, filePort);
                    if (!fc.isConnected()) {
                        System.err.println("Not connected.");
                        return;
                    }

                    System.out.println("Authenticating File Server...");
                    byte[] fingerprint = null;
                    try {
                        fingerprint = fc.authenticate(groupToken);
                    } catch(BadHandshakeException e){
                        System.out.println("Authentication failed: " + e.getMessage());
                        return;
                    } catch(ReadException e){
                        System.err.println("Unable to read message: " + e.getMessage());
                        e.printStackTrace(System.err);
                        return;
                    } catch(SendException e){
                        System.err.println("Unable to send message: " + e.getMessage());
                        e.printStackTrace(System.err);
                        return;
                    } catch(EOFException e){
                        System.out.println("Connection closed.");
                        return;
                    } catch(GeneralSecurityException e){
                        System.err.println("Got fatal security exception:");
                        e.printStackTrace(System.err);
                        return;
                    }

                    assert gc.isConnected() && gc.isAuthenticated();
                    System.out.println("Getting token for File Server...");
                    UserToken fileToken = gc.getToken(username, fingerprint);
                    System.out.println("Got token successfully.");
                    FileApp fa = new FileApp(fileServer, filePort, fileToken, fc);

                    fa.run();
                }

                else {
                    System.out.println("Unknown command.");
                }
            } else {
                System.err.println("No token was received.");
            }
        } else if(userChoice == 2){
            System.out.println("See you next time!");
            System.exit(0);
        } else {
            System.out.println("Unknown command.");
        }
    }
}
