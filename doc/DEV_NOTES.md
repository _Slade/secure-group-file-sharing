<meta charset="utf-8">

The project has the following architecture:

                     ┌──────────────────┐           ┌─────────────────────────┐
                     │ Group Client     │           │ Group Server            │
      1. Requests    ├──────────────────┤ Envelopes ├─────────────────────────┤
      UserToken ┌───>│ * Extends Client ├──────────>│ * Extends Server        │
                │    │ * Runs as part   │           │ * Runs on GroupThread   │
                │    │ of UserClient    │           │ * Run by RunGroupServer │
                │    │                  │           │ * Maintains a UserList  │
                │    └──────────────────┘           └──────────┬──┬───────────┘
                │    2. Get UserToken                          │  │
    ┌───────────┴─┐<───────────────────────────────────────────┘  │
    │ User Client │  5. Tell client result of file operation      │ 4. Check user
    └───────────┬─┘<───────────────────────────────────────────┐  │ permissions
                │                                              │  │
                │    ┌──────────────────┐           ┌──────────┴──┴──────────┐
                │    │ File Client      │           │ File Servers           │
                │    ├──────────────────┤ Envelopes ├────────────────────────┤
      3. Does   └───>│ * Extends Client ├──────────>│ * Extends Server       │
      file           │ * Runs as part   │           │ * Runs on FileThread   │
      operations     │ of UserClient    │           │ * Run by RunFileServer │
                     │                  │           │ * Maintains a FileList │
                     └──────────────────┘           └────────────────────────┘
